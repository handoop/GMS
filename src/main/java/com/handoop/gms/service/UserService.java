package com.handoop.gms.service;

import com.handoop.gms.domain.User;

/**
 * Created by Administrator on 2014/12/21.
 */
public interface UserService {

    User login(String account, String password);
}
