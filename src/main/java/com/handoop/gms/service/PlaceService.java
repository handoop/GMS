package com.handoop.gms.service;

import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.Place;

import java.util.List;

/**
 * Created by Administrator on 2014/12/24.
 */
public interface PlaceService {

    boolean createPlace(Place place);

    Page getPlacesByPage(int pageNum, Long type);

    List<Place> getPlacesByType(Long typeId);

    Place getPlaceByTypeAndLabel(Long typeId, String label);

    Place getPlaceById(Long id);

    boolean updatePlace(Place place);

    boolean deletePlace(Long placeId);
}
