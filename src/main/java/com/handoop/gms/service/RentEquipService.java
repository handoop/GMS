package com.handoop.gms.service;

import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.RentEquipment;

/**
 * Created by Administrator on 2015-1-6.
 */
public interface RentEquipService {

    boolean createRent(RentEquipment rent);

    Page getRentPage(int pageNum);
}
