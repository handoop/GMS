package com.handoop.gms.service;

import com.handoop.gms.domain.Type;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2014/12/24.
 */
public interface TypeService<T> {

    boolean createType(Type type);

    List findAllType(Class<T> clazz);

    T findUniqueType(Class<T> clazz, Long id);

    boolean deleteType(Class<T> clazz, Long id);

}
