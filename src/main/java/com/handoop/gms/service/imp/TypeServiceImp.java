package com.handoop.gms.service.imp;

import com.handoop.gms.dao.DaoSupport;
import com.handoop.gms.domain.Type;
import com.handoop.gms.service.TypeService;
import com.handoop.gms.utils.QueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015-1-4.
 */
@Service
@Transactional
public class TypeServiceImp<T> implements TypeService<T> {

    @Autowired private DaoSupport<T> daoSupport;

    @Override
    public boolean createType(Type type) throws RuntimeException{
        type.setCreateDate(new Date());
        int i = daoSupport.append(new QueryHelper()
                .setNameSpace(type.getClass().getSimpleName())
                .setSql_method("insert_one")
                .setValue(type));
        return i>0 ? true : false;
    }

    @Override
    public List findAllType(Class<T> clazz) throws RuntimeException {
        return daoSupport.findAll(new QueryHelper()
                .setNameSpace(clazz.getSimpleName())
                .setSql_method("select_all"));
    }

    @Override
    public T findUniqueType(Class<T> clazz, Long id) throws RuntimeException {
        return daoSupport.findUnique(new QueryHelper()
                .setNameSpace(clazz.getSimpleName())
                .setSql_method("select_one_by_id")
                .setValue(id));
    }

    @Override
    public boolean deleteType(Class<T> clazz, Long id) throws RuntimeException {
        int i = daoSupport.deleteOne(new QueryHelper()
                .setNameSpace(clazz.getSimpleName())
                .setSql_method("delete_one_by_id")
                .setValue(id));
        return i>0 ? true : false;
    }
}
