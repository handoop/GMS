package com.handoop.gms.service.imp;

import com.handoop.gms.config.Configuration;
import com.handoop.gms.dao.DaoSupport;
import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.RentPlace;
import com.handoop.gms.service.RentPlaceService;
import com.handoop.gms.utils.QueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2015-1-5.
 */
@Service
@Transactional
public class RentPlaceServiceImp implements RentPlaceService {

    @Autowired private DaoSupport<RentPlace> daoSupport;

    @Override
    public boolean createRent(RentPlace rent) {
        rent.setCreateDate(new Date());
        int i = daoSupport.append(new QueryHelper()
                .setNameSpace(RentPlace.class.getSimpleName())
                .setSql_method("insert_one")
                .setValue(rent));
        return i>0?true:false;
    }

    /**
     * 某个场地的某个时间段内是否有预约/预留冲突
     * @param placeId
     * @param begin
     * @param end
     * @return true表示有，false表示没有
     */
    @Override
    public boolean hasObligateConflict(Long placeId, Date begin, Date end) {
        int count = daoSupport.recordCount(new QueryHelper()
                .setNameSpace(RentPlace.class.getSimpleName())
                .setSql_method("count_obligate_conflict")
                .setParams("placeId", placeId)
                .setParams("begin", begin)
                .setParams("end", end));
        return count>0?true:false;
    }

    @Override
    public Page getPageRent(int pageNum) {
        List<RentPlace> rents = daoSupport.findAllByValue(new QueryHelper()
                .setNameSpace(RentPlace.class.getSimpleName())
                .setSql_method("select_by_complicated_condition")
                .setParams("index", (pageNum-1)* Configuration.DEFAULT_PAGESIZE)
                .setParams("size", Configuration.DEFAULT_PAGESIZE));
        int count = daoSupport.recordCount(new QueryHelper()
                .setNameSpace(RentPlace.class.getSimpleName())
                .setSql_method("select_by_complicated_condition_get_count"));
        return new Page(pageNum, Configuration.DEFAULT_PAGESIZE, count, rents);
    }

    /**
     * 批量插入
     * @param rents
     * @return
     */
    @Override
    public boolean batCreateRent(List<RentPlace> rents) {
        daoSupport.append(new QueryHelper()
                .setNameSpace(RentPlace.class.getSimpleName())
                .setSql_method("insert_bat")
                .setValue(rents));
        return true;
    }
}
