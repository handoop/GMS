package com.handoop.gms.service.imp;

import com.handoop.gms.config.Configuration;
import com.handoop.gms.dao.DaoSupport;
import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.Place;
import com.handoop.gms.service.PlaceService;
import com.handoop.gms.utils.QueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2014/12/25.
 */
@Service
@Transactional
public class PlaceServiceImp implements PlaceService {

    @Autowired
    private DaoSupport<Place> daoSupport;

    /**
     * 常见场地
     * @param place
     * @return
     * @throws RuntimeException
     */
    @Override
    public boolean createPlace(Place place)  throws RuntimeException{
        place.setCreateDate(new Date());
        int i = daoSupport.append(new QueryHelper()
                .setNameSpace(Place.class.getSimpleName())
                .setSql_method("insert_one")
                .setValue(place));
        return i>0?true:false;
    }

    /**
     * 分页获得场地的数据
     * @param pageNum
     * @param type
     * @return
     */
    @Override
    public Page getPlacesByPage(int pageNum, Long type) {
        List<Place> places = daoSupport.findAllByValue(new QueryHelper()
                .setNameSpace(Place.class.getSimpleName())
                .setSql_method("select_page")
                .setParams("type", type)
                .setParams("index", (pageNum - 1) * Configuration.DEFAULT_PAGESIZE)
                .setParams("size", Configuration.DEFAULT_PAGESIZE));
        Integer recordCount = daoSupport.recordCount(new QueryHelper()
                .setNameSpace(Place.class.getSimpleName())
                .setSql_method("select_page_record_count")
                .setValue(type));
        return new Page(pageNum, Configuration.DEFAULT_PAGESIZE, recordCount, places);
    }

    /**
     * 通过场地类型获得场地数据
     * @param typeId
     * @return
     */
    @Override
    public List<Place> getPlacesByType(Long typeId) {
        return daoSupport.findAllByValue(new QueryHelper()
                .setNameSpace(Place.class.getSimpleName())
                .setSql_method("select_by_type")
                .setValue(typeId));
    }

    /**
     * 通过类型和编号获得场地
     * @param typeId
     * @param label
     * @return
     */
    @Override
    public Place getPlaceByTypeAndLabel(Long typeId, String label) {
        return daoSupport.findUnique(new QueryHelper()
                .setNameSpace(Place.class.getSimpleName())
                .setSql_method("select_by_type_label")
                .setParams("typeId", typeId)
                .setParams("label", label));
    }

    @Override
    public Place getPlaceById(Long id) {
        return daoSupport.findUnique(new QueryHelper()
                .setNameSpace(Place.class.getSimpleName())
                .setSql_method("select_one_by_id")
                .setValue(id));
    }

    /**
     * 更新场地信息
     * @param place
     * @return
     */
    @Override
    public boolean updatePlace(Place place) {
        int result = daoSupport.updateUnique(new QueryHelper()
                .setNameSpace(Place.class.getSimpleName())
                .setSql_method("update_one")
                .setValue(place));
        return result>0?true:false;
    }

    @Override
    public boolean deletePlace(Long placeId) {
        return false;
    }


}
