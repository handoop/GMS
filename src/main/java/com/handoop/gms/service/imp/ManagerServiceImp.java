package com.handoop.gms.service.imp;

import com.handoop.gms.dao.DaoSupport;
import com.handoop.gms.domain.Manager;
import com.handoop.gms.domain.User;
import com.handoop.gms.service.ManagerService;
import com.handoop.gms.utils.QueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 2014-12-29.
 */
@Service
@Transactional
public class ManagerServiceImp implements ManagerService{

    public static final String MANAGER_NAME_SPACE = "Manager";
    @Autowired
    private DaoSupport<Manager> daoSupport;

    @Override
    public User login(String account, String password) throws RuntimeException {
        return daoSupport.findUnique(new QueryHelper()
                .setNameSpace(MANAGER_NAME_SPACE)
                .setSql_method("select_acc_pass")
                .setParams("account", account)
                .setParams("password", password));
    }
}
