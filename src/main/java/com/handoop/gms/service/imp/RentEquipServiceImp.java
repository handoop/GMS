package com.handoop.gms.service.imp;

import com.handoop.gms.config.Configuration;
import com.handoop.gms.dao.DaoSupport;
import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.RentEquipment;
import com.handoop.gms.service.RentEquipService;
import com.handoop.gms.utils.QueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2015-1-6.
 */
@Service
@Transactional
public class RentEquipServiceImp implements RentEquipService {


    @Autowired private DaoSupport<RentEquipment> daoSupport;

    @Override
    public boolean createRent(RentEquipment rent) {
        int result = daoSupport.append(new QueryHelper()
                .setNameSpace(RentEquipment.class.getSimpleName())
                .setSql_method("insert_one")
                .setValue(rent));
        return result>0?true:false;
    }

    @Override
    public Page getRentPage(int pageNum) {
        List<RentEquipment> rents = daoSupport.findAllByValue(new QueryHelper()
                .setNameSpace(RentEquipment.class.getSimpleName())
                .setSql_method("select_by_complicated_condition")
                .setParams("index", (pageNum-1)* Configuration.DEFAULT_PAGESIZE)
                .setParams("size", Configuration.DEFAULT_PAGESIZE));
        int count = daoSupport.recordCount(new QueryHelper()
                .setNameSpace(RentEquipment.class.getSimpleName())
                .setSql_method("select_by_complicated_condition_get_count"));
        return new Page(pageNum, Configuration.DEFAULT_PAGESIZE, count, rents);
    }

}
