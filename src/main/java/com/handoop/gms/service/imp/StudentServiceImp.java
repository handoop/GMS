package com.handoop.gms.service.imp;

import com.handoop.gms.config.Configuration;
import com.handoop.gms.dao.DaoSupport;
import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.Student;
import com.handoop.gms.service.StudentService;
import com.handoop.gms.utils.QueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2015-1-5.
 */
@Service
@Transactional
public class StudentServiceImp implements StudentService {

    @Autowired private DaoSupport<Student> daoSupport;

    @Override
    public Student getStudentByAccount(String account) {
        return daoSupport.findUnique(new QueryHelper()
                .setNameSpace(Student.class.getSimpleName())
                .setSql_method("select_one_by_account")
                .setValue(account));
    }

    @Override
    public Student getStudentByAccountAndPassword(String account, String password) {
        return daoSupport.findUnique(new QueryHelper()
                .setNameSpace(Student.class.getSimpleName())
                .setSql_method("select_one_by_account_password")
                .setParams("account", account)
                .setParams("password", password));
    }

    @Override
    public Page getStudentsPage(int pageNum) {
        List<Student> places = daoSupport.findAllByValue(new QueryHelper()
                .setNameSpace(Student.class.getSimpleName())
                .setSql_method("select_page")
                .setParams("index", (pageNum - 1) * Configuration.DEFAULT_PAGESIZE)
                .setParams("size", Configuration.DEFAULT_PAGESIZE));
        Integer recordCount = daoSupport.recordCount(new QueryHelper()
                .setNameSpace(Student.class.getSimpleName())
                .setSql_method("select_page_record_count"));
        return new Page(pageNum, Configuration.DEFAULT_PAGESIZE, recordCount, places);
    }

    @Override
    public boolean createStudent(Student student) {
        int result = daoSupport.append(new QueryHelper()
                .setNameSpace(Student.class.getSimpleName())
                .setSql_method("insert_all")
                .setValue(student));
        return result>0?true:false;
    }
}
