package com.handoop.gms.service.imp;

import com.handoop.gms.config.Configuration;
import com.handoop.gms.dao.DaoSupport;
import com.handoop.gms.domain.Equipment;
import com.handoop.gms.domain.Page;
import com.handoop.gms.service.EquipmentService;
import com.handoop.gms.utils.QueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015-1-4.
 */
@Service
@Transactional
public class EquipServiceImp implements EquipmentService {


    @Autowired private DaoSupport<Equipment> daoSupport;

    @Override
    public boolean createEquip(Equipment equip) {
        equip.setCreateDate(new Date());
        int result = daoSupport.append(new QueryHelper()
                .setNameSpace(Equipment.class.getSimpleName())
                .setSql_method("insert_one")
                .setValue(equip));
        return result>0?true:false;
    }

    @Override
    public Page getEquipPage(int pageNum, Long typeId, Integer status, String queryStr) {
        if (queryStr!=null){
            queryStr = queryStr.replaceAll("\\s+", "%");
            queryStr = "%"+queryStr+"%";
        }
        List<Equipment> places = daoSupport.findAllByValue(new QueryHelper()
                .setNameSpace(Equipment.class.getSimpleName())
                .setSql_method("select_page")
                .setParams("type", typeId)
                .setParams("status", status)
                .setParams("queryStr", queryStr)
                .setParams("index", (pageNum - 1) * Configuration.DEFAULT_PAGESIZE)
                .setParams("size", Configuration.DEFAULT_PAGESIZE));
        Integer recordCount = daoSupport.recordCount(new QueryHelper()
                .setNameSpace(Equipment.class.getSimpleName())
                .setSql_method("select_page_record_count")
                .setParams("type", typeId)
                .setParams("status", status)
                .setParams("queryStr", queryStr));
        return new Page(pageNum, Configuration.DEFAULT_PAGESIZE, recordCount, places);
    }

    @Override
    public List getEquipByType(Long typeId) {
        return daoSupport.findAllByValue(new QueryHelper()
                .setNameSpace(Equipment.class.getSimpleName())
                .setSql_method("select_by_type")
                .setValue(typeId));
    }

    @Override
    public Equipment getEquipById(Long equipId) {
        return daoSupport.findUnique(new QueryHelper()
                .setNameSpace(Equipment.class.getSimpleName())
                .setSql_method("select_one")
                .setValue(equipId));
    }
}
