package com.handoop.gms.service;

import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.RentPlace;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015-1-5.
 */
public interface RentPlaceService {
    boolean createRent(RentPlace rent);

    boolean hasObligateConflict(Long placeId, Date begin, Date end);

    Page getPageRent(int pageNum);

    boolean batCreateRent(List<RentPlace> rents);
}
