package com.handoop.gms.service;

import com.handoop.gms.domain.Competition;
import com.handoop.gms.domain.Page;

import java.util.Date;

/**
 * Created by Administrator on 2015-1-6.
 */
public interface ComService {
    boolean createCompetition(Competition comp);

    boolean createCompetition(Competition comp, Long[] placeId, Date[] begin, Date[] end);

    Page getCompPage(int pageNum);
}
