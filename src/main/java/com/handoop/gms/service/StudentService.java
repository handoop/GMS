package com.handoop.gms.service;

import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.Student;

/**
 * Created by Administrator on 2015-1-5.
 */
public interface StudentService {
    Student getStudentByAccount(String account);

    Student getStudentByAccountAndPassword(String account, String password);

    Page getStudentsPage(int pageNum);

    boolean createStudent(Student student);
}
