package com.handoop.gms.service;

import com.handoop.gms.domain.Equipment;
import com.handoop.gms.domain.Page;

import java.util.List;

/**
 * Created by Administrator on 2014/12/24.
 */
public interface EquipmentService {

    boolean createEquip(Equipment equip);

    Page getEquipPage(int pageNum, Long typeId, Integer status, String queryStr);

    List getEquipByType(Long typeId);

    Equipment getEquipById(Long equipId);
}
