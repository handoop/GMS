package com.handoop.gms.config;

/**
 * Created by Administrator on 2015-1-4.
 */
public final class Configuration {

    /**
     * 分页导航栏中允许显示多少页
     */
    public static final int PAGE_NAVIGATE = 5;
    /**
     * 一页显示多少场地信息
     */
    public static final int SHOW_PLACE_SIZE = 1;
    /**
     * 默认一页的数据量
     */
    public static final int DEFAULT_PAGESIZE = 2;
}
