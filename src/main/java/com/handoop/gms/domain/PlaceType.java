package com.handoop.gms.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Set;

/**
 * Created by Administrator on 2014/12/23.
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class PlaceType extends Type {

    private Set<Place> places;

    public Set<Place> getPlaces() {
        return places;
    }

    public void setPlaces(Set<Place> places) {
        this.places = places;
    }
}
