package com.handoop.gms.domain;

/**
 * Created by Administrator on 2014/12/23.
 */
public class RentEquipment extends Rent {

    private Student student;
    private Equipment equipment;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }
}
