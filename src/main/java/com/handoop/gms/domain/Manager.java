package com.handoop.gms.domain;

/**
 * Created by Administrator on 2014/12/23.
 */
public class Manager extends User {

    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
