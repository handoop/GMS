package com.handoop.gms.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/23.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Equipment {

    /**
     * 可租借
     */
    public static final int RENTABLE = 0;
    /**
     * 维修中
     */
    public static final int REMAINING = 1;
    /**
     * 不可租借
     */
    public static final int UNRENTABLE = 2;
    /**
     * 已被租借
     */
    public static final int RENTED = 3;

    private Long id;
    private String label;
    private String description;
    private int status;
    private Date createDate;
    private Float price;
    private EquipmentType type;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public EquipmentType getType() {
        return type;
    }

    public void setType(EquipmentType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
