package com.handoop.gms.domain;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/23.
 */
public class Type {

    protected Long id;
    protected String name;
    protected Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
