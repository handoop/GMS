package com.handoop.gms.domain;

/**
 * Created by Administrator on 2014/12/23.
 */
public class RentPlace extends Rent {

    private int level;
    private Place place;
    private Student student;
    private Competition competition;


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }
}
