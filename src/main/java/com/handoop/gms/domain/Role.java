package com.handoop.gms.domain;

import java.util.Date;
import java.util.Set;

/**
 * Created by Administrator on 2014/12/23.
 */
public class Role {


    private Long id;
    private String name;
    private Date createDate;
    private Set<Privilege> privileges;
    private Set<Manager> managers;

    public Set<Manager> getManagers() {
        return managers;
    }

    public void setManagers(Set<Manager> managers) {
        this.managers = managers;
    }

    public Set<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Set<Privilege> privileges) {
        this.privileges = privileges;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
