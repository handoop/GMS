package com.handoop.gms.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Set;

/**
 * Created by Administrator on 2014/12/23.
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class EquipmentType extends Type {

    private Set<Equipment> equipments;

    public Set<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(Set<Equipment> equipments) {
        this.equipments = equipments;
    }
}
