package com.handoop.gms.domain;

import java.util.Set;

/**
 * Created by Administrator on 2014/12/21.
 */
public class Student extends User{

    private Set<RentPlace> placeRents;
    private Set<RentEquipment> equipRents;

    public Set<RentPlace> getPlaceRents() {
        return placeRents;
    }

    public void setPlaceRents(Set<RentPlace> placeRents) {
        this.placeRents = placeRents;
    }

    public Set<RentEquipment> getEquipRents() {
        return equipRents;
    }

    public void setEquipRents(Set<RentEquipment> equipRents) {
        this.equipRents = equipRents;
    }
}
