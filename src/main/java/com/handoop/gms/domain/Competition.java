package com.handoop.gms.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Set;

/**
 * Created by Administrator on 2014/12/23.
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class Competition {

    private Long id;
    private String name;
    private String label;
    private int status;
    private String organizes;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date holdDate;
    private String principal;
    private String principalTele;
    private Set<RentPlace> rents;

    @Override
    public String toString() {
        return "Competition{" +
                "principalTele='" + principalTele + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", label='" + label + '\'' +
                ", status=" + status +
                ", organizes='" + organizes + '\'' +
                ", holdDate=" + holdDate +
                ", principal='" + principal + '\'' +
                '}';
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public Set<RentPlace> getRents() {
        return rents;
    }

    public void setRents(Set<RentPlace> rents) {
        this.rents = rents;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOrganizes() {
        return organizes;
    }

    public void setOrganizes(String organizes) {
        this.organizes = organizes;
    }

    public Date getHoldDate() {
        return holdDate;
    }

    public void setHoldDate(Date holdDate) {
        this.holdDate = holdDate;
    }

    public String getPrincipalTele() {
        return principalTele;
    }

    public void setPrincipalTele(String principalTele) {
        this.principalTele = principalTele;
    }

}
