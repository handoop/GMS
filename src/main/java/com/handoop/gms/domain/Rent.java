package com.handoop.gms.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/23.
 */
public class Rent {

    /**
     * 预约中
     */
    public static final int OBLIGATE = 0;
    /**
     * 使用中
     */
    public static final int USING = 1;
    /**
     * 交易完成
     */
    public static final int DONE = 2;
    /**
     * 违约
     */
    public static final int DEFAULT = 3;
    /**
     * 取消预约
     */
    public static final int CANCEL = 4;

    protected Long id;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date begin;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date end;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    private int status;
    private float publish;
    protected float wholePrice;
    protected String telephone;

    public float getPublish() {
        return publish;
    }

    public void setPublish(float publish) {
        this.publish = publish;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public float getWholePrice() {
        return wholePrice;
    }

    public void setWholePrice(float wholePrice) {
        this.wholePrice = wholePrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
