package com.handoop.gms.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;
import java.util.Set;

/**
 * Created by Administrator on 2014/12/23.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Place {

    private Long id;
    private String label;
    private int status;
    private Date createDate;
    private Float price;
    private String openTime;
    private PlaceType type;
    private Set<RentPlace> rents;


    @Override
    public String toString() {
        return "Place{" +
                "openTime='" + openTime + '\'' +
                ", id=" + id +
                ", label='" + label + '\'' +
                ", status=" + status +
                ", createDate=" + createDate +
                ", price=" + price +
                '}';
    }

    //可以使用
    public static final int STATUS_ING = 0;
    //暂时不开放
    public static final int STATUS_FORBID = 1;


    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }



    public Set<RentPlace> getRents() {
        return rents;
    }

    public void setRents(Set<RentPlace> rents) {
        this.rents = rents;
    }

    public PlaceType getType() {
        return type;
    }

    public void setType(PlaceType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
