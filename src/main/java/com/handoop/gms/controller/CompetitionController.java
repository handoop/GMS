package com.handoop.gms.controller;

import com.handoop.gms.domain.*;
import com.handoop.gms.service.ComService;
import com.handoop.gms.service.PlaceService;
import com.handoop.gms.service.RentPlaceService;
import com.handoop.gms.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015-1-2.
 */
@Controller
public class CompetitionController extends BaseController {

    private static final int COMPETITION = 100;
    private static final int CREATE = 101;
    private static final Object SEARCH_PLACE = 102;

    @Autowired private TypeService<PlaceType> ptService;
    @Autowired private ComService comService;


    /**
     * 所有赛事
     * @param model
     * @return
     */
    @RequestMapping("/manage/competition")
    public String competition(Model model, Integer pageNum){
        model.addAttribute("pageIndex", COMPETITION);
        Page page = comService.getCompPage(pageNum==null?1:pageNum);
        model.addAttribute("page", page);
        return "manage/competition/competition";
    }

    /**
     * 创建赛事的页面
     * @param model
     * @return
     */
    @RequestMapping(value = "/manage/competition/create", method = RequestMethod.GET)
    public String createComp(Model model, Competition comp){
        model.addAttribute(PAGE_INDEX, CREATE);
        model.addAttribute("types", ptService.findAllType(PlaceType.class));
        return "manage/competition/createComp";
    }

    /**
     * 创建赛事的表单处理
     * @return
     */
    @RequestMapping(value = "/manage/competition/create", method = RequestMethod.POST)
    public String createComp(Competition comp
            , Long[] placeId
            , @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date[] begin
            , @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date[] end){
        comService.createCompetition(comp, placeId, begin, end);
        return "redirect:/manage/competition/create";
    }

    /**
     * 查询的页面
     * @return
     */
    @RequestMapping(value = "/manage/competition/search", method = RequestMethod.GET)
    public String search(Model model){
        model.addAttribute("pageIndex", SEARCH_PLACE);
        return "manage/competition/searchComp";
    }

    /**
     * 查询处理
     * @param queryStr
     * @return
     */
    @RequestMapping(value = "/manage/competition/search", method = RequestMethod.POST)
    public String search(String queryStr, RedirectAttributes attr){
        System.out.println(queryStr);
        attr.addFlashAttribute("data", "1111111111111111111111111111111111111");
        attr.addFlashAttribute("queryStr", queryStr);
        return "redirect:/manage/competition/search";
    }

}
