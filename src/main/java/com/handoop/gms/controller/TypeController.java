package com.handoop.gms.controller;

import com.handoop.gms.domain.Equipment;
import com.handoop.gms.domain.EquipmentType;
import com.handoop.gms.domain.PlaceType;
import com.handoop.gms.domain.Type;
import com.handoop.gms.service.EquipmentService;
import com.handoop.gms.service.PlaceService;
import com.handoop.gms.service.TypeService;
import com.handoop.gms.service.imp.TypeServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Administrator on 2015-1-3.
 */
@Controller
public class TypeController extends BaseController {

    private static final int PLACE = 1;
    private static final int EQUIP = 2;
    public static final int SHOWPLACETYPE = 1002;
    public static final int SHOWEQUIPTYPE = 1003;

    @Autowired
    private TypeService<PlaceType> ptService;
    @Autowired
    private TypeService<EquipmentType> etService;


    /**
     * 场地类型
     * @param model
     * @return
     */
    @RequestMapping("/manage/type/place")
    public String showPlaceType(Model model){
        model.addAttribute(PAGE_INDEX, SHOWPLACETYPE);
        model.addAttribute("type", ptService.findAllType(PlaceType.class));
        return "manage/type/place_type";
    }

    /**
     * 器材类型
     * @param model
     * @return
     */
    @RequestMapping("/manage/type/equip")
    public String showEquipType(Model model){
        model.addAttribute(PAGE_INDEX, SHOWEQUIPTYPE);
        model.addAttribute("type", etService.findAllType(EquipmentType.class));
        return "manage/type/equip_type";
    }

    /**
     * 创建场地类型的页面
     * @param model
     * @param sign
     * @return
     */
    @RequestMapping(value = "/manage/type/create/{sign}", method = RequestMethod.GET)
    public String createType(Model model,@PathVariable int sign){
        switch (sign){
            case PLACE:
                model.addAttribute(PAGE_INDEX, SHOWPLACETYPE);
                return "manage/type/create_place_type";
            case EQUIP:
                model.addAttribute(PAGE_INDEX, SHOWEQUIPTYPE);
                return "manage/type/create_equip_type";
        }
        return "manage/type/create_place_type";
    }

    /**
     * 创建场地类型
     * @param sign
     * @param name
     * @return
     */
    @RequestMapping(value = "/manage/type/create/{sign}", method = RequestMethod.POST)
    public String createType(@PathVariable int sign, String name){
        Type type;
        boolean isOk;
        switch (sign){
            case PLACE:
                type = new PlaceType();
                type.setName(name);
                isOk = ptService.createType(type);
                return isOk ? "redirect:/manage/type/place" : "redirect:/home/404";
            case EQUIP:
                type = new EquipmentType();
                type.setName(name);
                isOk = etService.createType(type);
                return isOk ? "redirect:/manage/type/equip" : "redirect:/home/404";
            default:
                return "redirect:/home/404";
        }
    }

    /**
     * 删除类型
     * @param sign
     * @return
     */
    @RequestMapping("/manage/type/delete/{sign}")
    public String deleteType(@PathVariable int sign, Long typeId){
        boolean isOk;
        switch (sign){
            case PLACE:
                isOk = ptService.deleteType(PlaceType.class, typeId);
                return isOk ? "redirect:/manage/type/place" : "redirect:/home/404";
            case EQUIP:
                isOk = etService.deleteType(EquipmentType.class, typeId);
                return isOk ? "redirect:/manage/type/equip" : "redirect:/home/404";
            default:
                return "redirect:/home/404";
        }
    }

    @RequestMapping(value = "/manage/type/update/{sign}", method = RequestMethod.GET)
    public String updateType(Model model, @PathVariable int sign){
        switch (sign){
            case PLACE:
                model.addAttribute(PAGE_INDEX, SHOWPLACETYPE);
                return "manage/type/update_place_type";
            case EQUIP:
                model.addAttribute(PAGE_INDEX, SHOWEQUIPTYPE);
                return "manage/type/update_equip_type";
        }
        return "manage/type/update_equip_type";
    }

    @RequestMapping(value = "/manage/type/update/{sign}", method = RequestMethod.POST)
    public String updateType(String name, @PathVariable int sign){
        switch (sign){
            case PLACE:
                return "manage/type/update_equip_type";
            case EQUIP:
                return "redirect:/manage/update_place_type";
        }
        return "manage/type/update_equip_type";
    }

}
