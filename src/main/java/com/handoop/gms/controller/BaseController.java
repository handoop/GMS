package com.handoop.gms.controller;

/**
 * Created by Administrator on 2014-12-29.
 */
public class BaseController {

    public static final String MANAGER_SESSION = "manager";
    public static final String STUDENT_SESSION = "student";
    public static final String PAGE_INDEX = "pageIndex";

    public static final String PAGE_ERROR = "redirect:/home/404";

}
