package com.handoop.gms.controller;

import com.handoop.gms.domain.*;
import com.handoop.gms.service.EquipmentService;
import com.handoop.gms.service.RentEquipService;
import com.handoop.gms.service.StudentService;
import com.handoop.gms.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2015-1-3.
 */
@Controller
public class EquipController extends BaseController {


    private static final int INDEX = 200;
    private static final int CREATE = 201;
    private static final int SEARCH = 202;
    private static final int RENT = 203;
    private static final int RENT_DETAILS = 204;

    @Autowired private TypeService<EquipmentType> typeService;
    @Autowired private EquipmentService equipService;
    @Autowired private StudentService studentService;
    @Autowired private RentEquipService rentService;

    @RequestMapping("/manage/equipment")
    public String equip(Model model
            , Integer pageNum
            , Long typeId
            , Integer status
            , String queryStr){
        model.addAttribute(PAGE_INDEX, INDEX);
        List<EquipmentType> types = typeService.findAllType(EquipmentType.class);
        if (types!=null && types.size()>0){
            Page page = equipService.getEquipPage(pageNum==null?1:pageNum, typeId, status, queryStr);
            model.addAttribute("types", types);
            model.addAttribute("page", page);
            model.addAttribute("typeId", typeId);
            model.addAttribute("status", status);
            model.addAttribute("queryStr", queryStr);
            return "manage/equipment/equipment";
        }else {
            return "manage/equipment/equipment";
        }
    }

    /**
     * 创建器材的页面
     * @param model
     * @return
     */
    @RequestMapping(value = "/manage/equipment/create", method = RequestMethod.GET)
    public String createComp(Model model, Equipment equip){
        model.addAttribute(PAGE_INDEX, CREATE);
        List<EquipmentType> types = typeService.findAllType(EquipmentType.class);
        model.addAttribute("types", types);
        return "manage/equipment/createEquip";
    }

    /**
     * 创建器材的表单处理
     * @return
     */
    @RequestMapping(value = "/manage/equipment/create", method = RequestMethod.POST)
    public String createComp(Equipment equip, Long typeId){
        if (typeId!=null){
            EquipmentType type = typeService.findUniqueType(EquipmentType.class, typeId);
            if (type!=null){
                equip.setType(type);
                equipService.createEquip(equip);
            }
        }
        return "redirect:/manage/equipment/create";
    }

    /**
     * 查询的页面
     * @return
     */
    @RequestMapping(value = "/manage/equipment/search", method = RequestMethod.GET)
    public String search(Model model){
        model.addAttribute(PAGE_INDEX, SEARCH);
        return "manage/equipment/searchEquip";
    }

    /**
     * 查询处理
     * @param queryStr
     * @return
     */
    @RequestMapping(value = "/manage/equipment/search", method = RequestMethod.POST)
    public String search(String queryStr, RedirectAttributes attr){
        System.out.println(queryStr);
        attr.addFlashAttribute("data", "1111111111111111111111111111111111111");
        attr.addFlashAttribute("queryStr", queryStr);
        return "redirect:/manage/equipment/searchEquip";
    }

    /**
     * 租借器材
     * @param model
     * @return
     */
    @RequestMapping(value = "/manage/equipment/rent", method = RequestMethod.GET)
    public String rent(Model model){
        model.addAttribute(PAGE_INDEX, RENT);
        List<EquipmentType> types = typeService.findAllType(EquipmentType.class);
        model.addAttribute("types", types);
        return "manage/equipment/rentEquip";
    }

    @RequestMapping(value = "/manage/equipment/rent", method = RequestMethod.POST)
    public String rent(Long equipId, String account, String telephone){
        Equipment equip = equipService.getEquipById(equipId);
        Student student = studentService.getStudentByAccount(account);
        if (equip!=null && student!=null){
            RentEquipment rent = new RentEquipment();
            rent.setEquipment(equip);
            rent.setStudent(student);
            rent.setCreateDate(new Date());
            rent.setBegin(new Date());
            rent.setTelephone(telephone);
            rentService.createRent(rent);
        }
        return "redirect:/manage/equipment/rent";
    }

    @RequestMapping("/equipment/obtainLabel")
    @ResponseBody
    public Map obtainLabel(Long typeId){
        if (typeId!=null){
            EquipmentType type = typeService.findUniqueType(EquipmentType.class, typeId);
            if (type!=null){
                List<Equipment> equip = equipService.getEquipByType(typeId);
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("response", true);
                map.put("equips", equip);
                return map;
            }else return null;
        }else return null;
    }

    @RequestMapping("/manage/equipment/rentDetails")
    public String rentDetails(Model model, Integer pageNum){
        model.addAttribute(PAGE_INDEX, RENT_DETAILS);
        Page page = rentService.getRentPage(pageNum==null?1:pageNum);
        model.addAttribute("page", page);
        return "manage/equipment/rentDetails";
    }



}
