package com.handoop.gms.controller;

import com.handoop.gms.domain.*;
import com.handoop.gms.service.PlaceService;
import com.handoop.gms.service.RentPlaceService;
import com.handoop.gms.service.StudentService;
import com.handoop.gms.service.TypeService;
import com.handoop.gms.utils.TextTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014-12-29.
 */
@Controller
public class PlaceController extends BaseController{


    private static final int SHOW_PLACE = 1;
    private static final int CREATE_PLACE = 2;
    private static final int SEARCH_PLACE = 3;
    private static final int OBLIGATE_PLACE = 4;
    private static final int OBLIGATE_DETAILS = 5;

    @Autowired private PlaceService placeService;
    @Autowired private TypeService<PlaceType> ptService;
    @Autowired private StudentService studentService;
    @Autowired private RentPlaceService rentService;

    /**
     * 所有场地
     * @param model
     * @return
     */
    @RequestMapping(value = "/manage/place/{type}")
    public String showPlace(Model model, Integer pageNum, @PathVariable Long type){
        model.addAttribute(PAGE_INDEX, SHOW_PLACE);
        List<PlaceType> types = ptService.findAllType(PlaceType.class);
        if (type==null||type==0){
            type = types.get(0).getId();
        }
        Page page = placeService.getPlacesByPage(pageNum==null?1:pageNum, type);
        model.addAttribute("types", types);
        model.addAttribute("type", type);
        model.addAttribute("page", page);
        return "manage/place/place";
    }

    /**
     * 添加场地的页面
     * @param model
     * @param place
     * @return
     */
    @RequestMapping(value = "/manage/place/createPlace", method = RequestMethod.GET)
    public String createPlace(Model model, Place place){
        model.addAttribute(PAGE_INDEX, CREATE_PLACE);
        model.addAttribute("type", ptService.findAllType(PlaceType.class));
        return "manage/place/createPlace";
    }

    /**
     * 添加场地的表单处理
     * @param model
     * @return
     */
    @RequestMapping(value = "/manage/place/createPlace", method = RequestMethod.POST)
    public String createPlace(Place place, Long typeId, Model model){
        System.out.println(place.toString());
        if (typeId!=null){
            PlaceType type = ptService.findUniqueType(PlaceType.class, typeId);
            if (type!=null){
                place.setType(type);
                place.setOpenTime(place.getOpenTime().trim());
                boolean isOk = placeService.createPlace(place);
                return isOk ? "redirect:/manage/place/createPlace" : PAGE_ERROR;
            }else {
                return PAGE_ERROR;
            }
        }else {
            return PAGE_ERROR;
        }
    }

    /**
     * 查询的页面
     * @return
     */
    @RequestMapping(value = "/manage/place/search", method = RequestMethod.GET)
    public String search(Model model){
        model.addAttribute(PAGE_INDEX, SEARCH_PLACE);
        return "manage/place/search";
    }

    /**
     * 查询处理
     * @param queryStr
     * @return
     */
    @RequestMapping(value = "/manage/place/search", method = RequestMethod.POST)
    public String search(String queryStr, RedirectAttributes attr){
        System.out.println(queryStr);
        attr.addFlashAttribute("data", "1111111111111111111111111111111111111");
        attr.addFlashAttribute("queryStr", queryStr);
        return "redirect:/manage/place/search";
    }

    /**
     * 预约页面
     * @param model
     * @param rent
     * @return
     */
    @RequestMapping(value = "/manage/place/obligate", method = RequestMethod.GET)
    public String obligate(Model model, RentPlace rent){
        model.addAttribute(PAGE_INDEX, OBLIGATE_PLACE);
        List<PlaceType> types = ptService.findAllType(PlaceType.class);
        model.addAttribute("types", types);
        return "/manage/place/obligate";
    }

    /**
     * 处理预约的表单处理
     * @param rent 实体模型
     * @param account 学生账号
     * @return
     */
    @RequestMapping(value = "/manage/place/obligate", method = RequestMethod.POST)
    public String obligate(RentPlace rent
            , Long placeId
            , String account){
        Place place = placeService.getPlaceById(placeId);
        if (place!=null){
            rent.setPlace(place);
            if (!TextTools.isEmpty(account)){
                Student student =studentService.getStudentByAccount(account);
                rent.setStudent(student);
            }
            boolean isOk = rentService.createRent(rent);
        }else {
            return PAGE_ERROR;
        }
        return "redirect:/manage/place/obligate";
    }

    /**
     * 前端选择类型，返回场地编号json
     * @param typeId
     * @return
     */
    @RequestMapping("/place/obligateLabel")
    @ResponseBody
    public Map obligateLabel(Long typeId){
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("-----------------"+typeId);
        if (typeId!=null){
            List<Place> places = placeService.getPlacesByType(typeId);
            map.put("response", true);
            map.put("places", places);
            System.out.println("run return");
            return map;
        }
        return null;
    }

    @RequestMapping("/manage/place/obligateDetails")
    public String obligateDetails(Model model, Integer pageNum){
        model.addAttribute(PAGE_INDEX, OBLIGATE_DETAILS);
        Page page = rentService.getPageRent(pageNum==null?1:pageNum);
        model.addAttribute("page", page);
        return "manage/place/obligateDetails";
    }

    /**
     * 预约是否有冲突
     * @param placeId
     * @param begin
     * @param end
     * @return  true表示没有，可以通过，false表示有冲突，不能提交
     */
    @RequestMapping("/place/obligateValDate")
    @ResponseBody
    public boolean obligateValDate(Long placeId
            , @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date begin
            , @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date end){
        return !rentService.hasObligateConflict(placeId, begin, end);
    }
    
    @RequestMapping(value = "/manage/place/update/{id}", method = RequestMethod.GET)
    public String update(Model model, Place place, @PathVariable Long id){
        model.addAttribute(PAGE_INDEX, SHOW_PLACE);
        model.addAttribute("type", ptService.findAllType(PlaceType.class));
        if (id!=null){
            place = placeService.getPlaceById(id);
            model.addAttribute("place", place);
            if (place!=null){
                return "manage/place/updatePlace";
            }else return PAGE_ERROR;
        }else return PAGE_ERROR;
    }

    @RequestMapping(value = "/manage/place/update", method = RequestMethod.POST)
    public String update(Place place, Long typeId){
        PlaceType type = ptService.findUniqueType(PlaceType.class, typeId);
        place.setType(type);
        boolean isOk = placeService.updatePlace(place);
        return isOk ? "redirect:/manage/place/"+typeId : PAGE_ERROR;
    }

    @RequestMapping("/manage/place/delete")
    public String delete(Long placeId){
        boolean isOk = placeService.deletePlace(placeId);
        return "redirect:/manage/place/0";
    }

}
