package com.handoop.gms.controller;

import com.handoop.gms.domain.Page;
import com.handoop.gms.domain.Student;
import com.handoop.gms.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by Administrator on 2014/12/24.
 */
@Controller
public class StudentController extends BaseController {


    private static final int STUDENT = 2200;

    @Autowired private StudentService studentService;

    /**
     * 学生登陆
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(String account, String password, HttpServletRequest request){     /*forward和redirect都会清空errors的值*/
        Student student = studentService.getStudentByAccountAndPassword(account, password);
        request.getSession().setAttribute(STUDENT_SESSION, student);
        return "redirect:/";
    }

    /**
     * 学生登陆页面
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(){
        return "customer/student/login";
    }

    @RequestMapping("/validLogin")
    @ResponseBody
    public boolean validLogin(String account, String password){
        Student student = studentService.getStudentByAccountAndPassword(account, password);
        return student==null?false:true;
    }

    @RequestMapping("/manage/student")
    public String student(Model model, Integer pageNum){
        model.addAttribute(PAGE_INDEX, STUDENT);
        Page page = studentService.getStudentsPage(pageNum==null?1:pageNum);
        model.addAttribute("page", page);
        return "/manage/student/student";
    }

    @RequestMapping("/manage/student/register")
    public String create(Student student){
        studentService.createStudent(student);
        return "redirect:/manage/student";
    }




}
