package com.handoop.gms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 2014/12/24.
 */
@Controller
public class HomeController {

    @RequestMapping(value = {"/index.do", "/"})
    public String home(){
        return "customer/home/home";
    }

    @RequestMapping("/manage")
    public String manageHome(){
        return "manage/home/index";
    }

    @RequestMapping("/home/404")
    public String error(){
        return "public/404";
    }




}
