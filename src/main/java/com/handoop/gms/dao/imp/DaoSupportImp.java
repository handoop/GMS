package com.handoop.gms.dao.imp;

import com.handoop.gms.dao.DaoSupport;
import com.handoop.gms.domain.Type;
import com.handoop.gms.utils.QueryHelper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2014/12/21.
 */
@Repository
public class DaoSupportImp<T> implements DaoSupport<T> {

    @Autowired
    private SqlSessionFactory sessionFactory;

    private SqlSession getSession(){
        return sessionFactory.openSession();
    }

    /**
     * 添加对象到数据库
     * @param helper    sql辅助类
     * @return
     */
    @Override
    public int append(QueryHelper helper) {
        return getSession().insert(helper.getSqlAction(), helper.getValue());
    }

    /**
     * 查询单个对象的数据
     * @param helper
     * @return
     */
    @Override
    public  T findUnique(QueryHelper helper) {
        return getSession().selectOne(helper.getSqlAction(), helper.getValue());
    }

    /**
     * 查询所有
     * @param helper
     * @return
     */
    @Override
    public List<T> findAll(QueryHelper helper) {
        return getSession().selectList(helper.getSqlAction());
    }

    /**
     * 删除一个数据
     * @param helper
     * @return
     */
    @Override
    public int deleteOne(QueryHelper helper) {
        return getSession().delete(helper.getSqlAction(), helper.getValue());
    }

    /**
     * 获得分页数据
     * @param helper
     * @return
     */
    @Override
    public List<T> findAllByValue(QueryHelper helper) {
        return getSession().selectList(helper.getSqlAction(), helper.getValue());
    }

    @Override
    public int recordCount(QueryHelper helper) {
        return getSession().selectOne(helper.getSqlAction(), helper.getValue());
    }

    @Override
    public int updateUnique(QueryHelper helper) {
        return getSession().update(helper.getSqlAction(), helper.getValue());
    }
}
