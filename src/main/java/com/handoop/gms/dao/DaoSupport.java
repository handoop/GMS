package com.handoop.gms.dao;

import com.handoop.gms.domain.Place;
import com.handoop.gms.domain.Type;
import com.handoop.gms.utils.QueryHelper;

import java.util.List;

/**
 * Created by Administrator on 2014/12/21.
 */
public interface DaoSupport<T> {

    int append(QueryHelper helper);

    T findUnique(QueryHelper helper);

    List<T> findAll(QueryHelper helper);

    int deleteOne(QueryHelper helper);

    List<T> findAllByValue(QueryHelper helper);

    int recordCount(QueryHelper helper);

    int updateUnique(QueryHelper helper);
}
