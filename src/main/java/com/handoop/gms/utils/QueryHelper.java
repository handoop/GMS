package com.handoop.gms.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/21.
 */
public class QueryHelper {

    private String name_space;
    private String sql_method;
    private Object value;
    private Map<String, Object> params;


    /**
     *
     * @param name_space mapper的名称空间
     */
    public QueryHelper setNameSpace(String name_space) {
        this.name_space = name_space;
        return this;
    }

    /**
     *
     * @param sql_method 具体的sql执行动作
     */
    public QueryHelper setSql_method(String sql_method) {
        this.sql_method = sql_method;
        return this;
    }

    /**
     *
     * @param value 参数，单个参数的情况
     */
    public QueryHelper setValue(Object value) {
        this.value = value;
        return this;
    }

    /**
     * 多个参数的情况
     * @param key 键值
     * @param value 值
     */
    public QueryHelper setParams(String key, Object value){
        if (params == null){
            params = new HashMap<String, Object>();
        }
        params.put(key, value);
        return this;
    }

    public Object getValue(){
        return params == null ? value : params;
    }

    /**
     * 完整的sql动作，包括名称空间和sql动作
     * @return
     */
    public String getSqlAction(){
        return name_space + "." + sql_method;
    }

}
