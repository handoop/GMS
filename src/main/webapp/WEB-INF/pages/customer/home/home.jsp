
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>首页</title>
    <link href="${pageContext.request.contextPath}/css/handoop.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>

</head>
<body>


<div class="header">

    <!--导航1-->
    <div class="hNavBar">
        <div class="wrap">
            <span style="font-size: 40px">广东海洋大学体育馆</span>
        </div>
        <div class="search">
            <div class="input-group"  style="float: right;">
                <input type="text" class="form-control" placeholder="search">
                <span class="input-group-addon">search</span>
            </div>
        </div>
    </div>

    <!--导航2-->
    <div class="wrap">
        <div class="navLogo">

        </div>
    </div>
</div>



<!--轮播图-->
<div id="myCarousel" class="carousel slide">
    <!-- 轮播（Carousel）指标 -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>
    <!-- 轮播（Carousel）项目 -->
    <div class="carousel-inner">
        <div class="item active">
            <img style="width: 100%; height: 500px" src="${pageContext.request.contextPath}/images/1.jpg" alt="First slide">
        </div>
        <div class="item">
            <img style="width: 100%; height: 500px" src="${pageContext.request.contextPath}/images/2.jpg" alt="Second slide">
        </div>
    </div>
</div>
<!--end-->

<div class="wrap">
    <p class="stream-title">
        <a href="#">最近动态  »</a>
    </p>
    <div class="stream">
        <ul class="stream-list">
            <li>
                <a href="passage.html">北大教授XXX莅临我校开讲座</a>
                <p>11月29日，下午14:30，多功能厅</p>
            </li>
            <li>
                <a>海大一年一度的二手交易市场</a>
                <p>11月25日，8:30-20:00，中心广场</p>
            </li>
            <li>
                <a>校级宿舍大检</a>
                <p>11月25日，15:00,</p>
            </li>
            <li>
                <a>腾讯公司CEO麻花藤莅临演讲</a>
                <p>11月23日，下午14:30</p>
            </li>
            <li>
                <a>Google公司联合创始人拉里·佩奇莅临我校演讲Google概念未来</a>
                <p>11月20日，下午14:30</p>
            </li>
            <li style="display: none">
                <a>Google公司联合创始人拉里·佩奇莅临我校演讲Google概念未来</a>
                <p>11月20日，下午14:30</p>
            </li>
            <li style="display: none">
                <a>Google公司联合创始人拉里·佩奇莅临我校演讲Google概念未来</a>
                <p>11月20日，下午14:30</p>
            </li>
        </ul>
        <div class="stream-footer">
            <div class="goto" style="left: 0px; top: 20px">></div>
            <div class="goto" style="left: 0px; top: 55px"><</div>
        </div>
    </div>

    <div class="filter-bar">
        <div class="filter" data-list="toggle">所有动态
            <ul class="filter-list" data-list="menu">
                <li><a href="channel.html">班级动态</a></li>
                <li><a href="#">艺术与文化</a></li>
                <li><a href="#">校园与交流</a></li>
                <li><a href="#">健康与医学</a></li>
                <li><a href="#">科学与工程</a></li>
                <li><a href="#">教学创新</a></li>
                <li><a href="#">校园公告</a></li>
                <li><a href="#">社会资讯</a></li>
            </ul>
            <span class="caret"></span>
        </div>
    </div>

    <div>
        <ul class="listing">
            <li class="listing-first">
                <p class="theme-more"><a href="#" >校园与交流 -></a></p>
                <p class="conTitle"><a href="#" >研究生“新生杯”篮球赛顺利举行</a></p>
                <p class="con">伴着明媚的阳光和凉爽的秋风，我校研究生“新生杯”篮球赛于15-16日在湖光校区教工篮球场进行。
                    15日下午，在小组赛中，水产学院新生队与经管学院&思政部新生联队各自凭借着较强的实力，携手晋级决赛。
                    在16日下午举行的冠军争夺战中，双方各自展示了夺冠的信心和顽强的斗志，从比赛伊始就陷入紧张激烈的争夺。
                    经管学院&思政部新生联队上半场发挥出色<a href="#" class="read-more">更多...</a></p>
                <span class="data">10分钟前</span>
            </li>
            <li>
                <p class="theme-more"><a href="channel.html" >艺术与文化 -></a></p>
                <p class="conTitle"><a href="#" >艺术学院学生参加2014年"千帆会杯"国际标准舞锦标赛创佳绩</a></p>
                <p class="con"><img class="inner-image" src="images/20141111102656585.jpg">11月5至8日，在舞蹈系高超、张子浩、高翔三位老师的带领、指导下，
                    中歌艺术学院组织了22名学生代表赴深圳参加了。<a href="#" class="read-more">更多...</a></p>
                <span class="data">25分钟前</span>
            </li>
            <li>
                <p class="theme-more"><a href="#" >科学与工程 -></a></p>
                <p class="conTitle"><a href="passage.html" >英国兰卡斯特大学张大奕教授应邀到我校作学术报告</a></p>
                <p class="con"><img class="inner-image" src="images/20141114041057942.jpg">
                    11月10日下午，英国兰卡斯特大学张大奕教授应邀到我校在兴农楼109教室为农学院师生作题为“新型环境生物技术<a href="#" class="read-more">更多...</a></p>
                <span class="data">30分钟前</span>
            </li>
            <li class="listing-first">
                <p class="theme-more"><a href="#" >体育与医学 -></a></p>
                <p class="conTitle"><a href="#" >体育与休闲学院举办公开课观摩活动</a></p>
                <p class="con">6月7日上午，体育与休闲学院在主楼课室举办了2013年度示范课理论课教学研讨观摩活动。学校督导组傅光老师亲临指导，学院领导和部分教师参加了观摩活动。
                    示范课上，曲进老师从滨海休闲现状、发展前景、存在问题等方面展开授课。王海燕老师主讲了肌肉活动能量供应方面的内容。在授课过程中<a href="#" class="read-more">更多...</a></p>
                <span class="data">1小时前</span>
            </li>
            <li>
                <p class="theme-more"><a href="#" >社会咨询 -></a></p>
                <p class="conTitle"><a href="#" >转发国务院办公厅关于做好2013年全国普通高等学校毕业生就业工作的通知</a></p>
                <p class="con">普通高等学校毕业生(以下简称高校毕业生)是国家宝贵的人才资源。做好高校毕业生就业工作，关乎经济发展、
                    民生改善和社会稳定。2013年，全国高校毕业生就业总量压力继续加大，结构性矛盾十分突出，就业任务更加繁重。
                    党中央、国务院高度重视高校毕业生就业工作，要求采取切实有效的措施，进一步做好高校毕业生就业工作。经国务院同意，<a href="#" class="read-more">更多...</a></p>
                <span class="data">1小时前</span>
            </li>
            <li><p class="theme-more"><a href="#" >班级动态 -></a></p>
                <p class="conTitle"><a href="#" >班委决定要去湖光岩烧烤啦！</a></p>
                <p class="con">天气渐渐冷了下来，这时候正是烧烤的好时机！机智的班委抓住时机跟几个班干关于户外活动的事宜深入的探讨，最终决定
                    这个星期六在湖光岩进行一次户外烧烤，班委XXX为了感谢大家一直以来对他工作的支持以及肯定，决定这次的活动自掏腰包，此次举动
                    迎来了全班的欢呼~<a href="#" class="read-more">更多...</a></p>
                <span class="data">1小时前</span></li>
            <li class="listing-first hidden">a</li>
            <li class="hidden">a</li>
            <li class="hidden">a</li>
        </ul>
        <div class="clearfix"></div>
        <div class="more">更多<div class="caret"></div></div>

    </div>

    <div class="clearfix"></div>



</div>



<div class="footer"></div>


<script src="js/handoop.js"></script>
<script src="js/picketColor.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $("[data-list='toggle']").dropMenu();
//            $("[data-role='carousel']").hCarousel();

        $('.more').on('click', function () {
            $('.listing').find(".hidden").each(function () {
                $(this).removeClass('hidden');
            });
        })
    });

</script>
</body>
</html>