<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 2:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags"  prefix="mvc"%>
<%@taglib uri="http://www.springframework.org/tags/form"  prefix="fo"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>

<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>登陆</title>
    <link href="${pageContext.request.contextPath}/css/handoop.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}js/jquery.validate.min.js"></script>
    <style type="text/css">
        .error{
            color:red;
            font-size: 10px;
        }
    </style>
</head>
<body>
<div  class="heroBody"><img src="${pageContext.request.contextPath}/images/heroshot.jpg"></div>
<div class="hNavBar">
    <div class="wrap">

    </div>
</div>
<div class="cForm-op"></div>
<div class="cForm">
    <fo:form id="userForm" action="/login" method="post" cssClass="form-horizontal" role="form">
        <div class="form-group">
            <label for="account" class="col-sm-3 control-label">账号：</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="account" name="account" placeholder="请输入账号">
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">密码：</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="password" name="password"  placeholder="请输入密码">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-10">
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> 请记住我
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-10">
                <button type="submit" class="btn btn-default">登录</button>
            </div>
        </div>
    </fo:form>
</div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        $("#userForm").validate({
            rules : {
                account:{
                    required : true
                },
                password : {
                    required : true,
                    remote : {
                        url : "/validLogin",
                        type : "post",
                        dataType : "json",
                        data : {
                            account : function () {
                                return $("#account").val();
                            },
                            password : function () {
                                return $("#password").val();
                            }
                        }
                    }
                }
            },
        messages : {
            account : {
                required : "账号不为空"
            },
            password : {
                required : "密码不能为空",
                remote : "账号或密码错误"
            }
        }
        })
    });

</script>
</html>

