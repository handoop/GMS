<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>租借器材</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage">主页</a></li>
                <li>器材管理</li>
                <li class="active">租借器材</li>
            </ol>
        </div>
        <div class="rwo">
            <div style="width: 600px; margin: 0 auto;padding: 20px 30px;background-color: #FFFFFF; border-radius: 5px">
                <fo:form action="/manage/equipment/rent" method="post" cssClass="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="typeId" class="col-sm-3 control-label">器材类型：</label>
                        <div class="col-sm-5">
                            <select  id="typeId" class="form-control">
                                <option value="">请选择类型</option>
                                <jstl:forEach items="${types}" var="item">
                                    <option value="${item.id}">${item.name}</option>
                                </jstl:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="label" class="col-sm-3 control-label">器材编号：</label>
                        <div class="col-sm-5">
                            <select  id="label" name="equipId" class="form-control">

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="account" class="col-sm-3 control-label">学生学号：</label>
                        <div class="col-sm-5">
                            <input class="form-control" id="account" name="account" placeholder="学生学号"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telephone" class="col-sm-3 control-label">联系电话：</label>
                        <div class="col-sm-5">
                            <input class="form-control" id="telephone" name="telephone" placeholder="联系电话"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">提交</button>
                        </div>
                    </div>
                </fo:form>
            </div>
        </div>
    </div>
</div>
</body>
<script style="text/javascript" src="${pageContext.request.contextPath}/js/rent_e.js"></script>
</html>
