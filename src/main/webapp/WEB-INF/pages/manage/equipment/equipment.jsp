<%@ taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>所有器材</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage">主页</a></li>
                <li>器材管理</li>
                <li class="active">所有器材</li>
            </ol>
        </div>
        <div class="wrap-table">
            <nav class="navbar navbar-default" role="navigation">
                <div>
                    <fo:form action="/manage/equipment" cssClass="navbar-form navbar-left" role="search" id="equipmentForm"  method="post">
                        <div class="input-group">
                            <span class="input-group-addon">器材类型</span>
                            <select  name="typeId" id="type" class="form-control">
                                <option ${typeId==null?'selected':''} value="">全部</option>
                                <jstl:forEach items="${types}" var="item">
                                    <option value="${item.id}">${item.name}</option>
                                </jstl:forEach>
                            </select>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">状态</span>
                            <select  name="status" id="status" class="form-control">
                                <option value="">全部</option>
                                <option value="0">可租借</option>
                                <option value="1">维修中</option>
                                <option value="2">不可租借</option>
                                <option value="3">已被租借</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="queryStr" placeholder="输入查询条件" value="${queryStr}">
                        </div>
                        <button type="submit" class="btn btn-default">搜索</button>
                    </fo:form>
                </div>
            </nav>
            <table class="table table-bordered table-striped table-hover table-simple">
                <thead>
                <tr>
                    <th>id</th>
                    <th>编号</th>
                    <th>收费标准</th>
                    <th>类型</th>
                    <th>状态</th>
                    <th>描述</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <jstl:forEach items="${page.recordList}" var="item">
                    <tr>
                        <td>${item.id}</td>
                        <td>${item.label}</td>
                        <td>${item.price}</td>
                        <td>${item.type.name}</td>
                        <td>
                            <jstl:choose>
                                <jstl:when test="${item.status==0}">可租借</jstl:when>
                                <jstl:when test="${item.status==1}">维修中</jstl:when>
                                <jstl:when test="${item.status==2}">不可租借</jstl:when>
                                <jstl:when test="${item.status==3}">已被租借</jstl:when>
                            </jstl:choose>
                        </td>
                        <td>${item.description}</td>
                        <td>
                            <button>更新</button>
                            <button>删除</button>
                        </td>
                    </tr>
                </jstl:forEach>
                </tbody>
            </table>
            <div class="col-sm-12" style="text-align: center">
                <ul class="pagination">
                    <jstl:if test="${page.currentPage>1}">
                        <li>
                            <a href="${pageContext.request.contextPath}/manage/equipment?pageNum=${page.currentPage-1}
                            &typeId=${typeId}&status=${status}&queryStr=${queryStr}">&laquo;</a>
                        </li>
                    </jstl:if>
                    <jstl:if test="${page.beginIndex>2}">
                        <li><a href="${pageContext.request.contextPath}/manage/equipment?pageNum=1&typeId=${typeId}&status=${status}&queryStr=${queryStr}">1</a></li>
                        <li><a href="#">...</a></li>
                    </jstl:if>
                    <jstl:forEach begin="${page.beginIndex}" end="${page.endIndex}" varStatus="step">
                        <li class="${page.currentPage==step.index?'active':''}">
                            <a href="${pageContext.request.contextPath}/manage/equipment?pageNum=${step.index}&typeId=${typeId}&status=${status}&queryStr=${queryStr}">${step.index}</a>
                        </li>
                    </jstl:forEach>
                    <jstl:if test="${page.endIndex+1<page.pageCount}">
                        <li><a href="#">...</a></li>
                        <li><a href="${pageContext.request.contextPath}/manage/equipment?pageNum=${page.pageCount}&typeId=${typeId}&status=${status}&queryStr=${queryStr}">${page.pageCount}</a></li>
                    </jstl:if>
                    <jstl:if test="${page.currentPage<page.pageCount}">
                        <li><a href="${pageContext.request.contextPath}/manage/equipment?pageNum=${page.currentPage+1}&typeId=${typeId}&status=${status}&queryStr=${queryStr}">&raquo;</a></li>
                    </jstl:if>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
