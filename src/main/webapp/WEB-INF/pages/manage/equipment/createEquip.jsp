<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>添加器材</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage/place">主页</a></li>
                <li>器材管理</li>
                <li class="active">添加器材</li>
            </ol>
        </div>
        <div class="row">
            <div style="width: 600px; margin: 0 auto; background-color: #ffffff; padding: 20px 30px; border-radius: 5px;">
                <fo:form action="/manage/equipment/create" cssClass="form-horizontal" role="form" modelAttribute="equipment">
                    <div class="form-group">
                        <label for="label" class="col-sm-3 control-label">编号：</label>
                        <div class="col-sm-5">
                            <fo:input cssClass="form-control" id="label" placeholder="请输入编号" path="label"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-sm-3 control-label">收费标准：</label>
                        <div class="col-sm-5">
                            <fo:input cssClass="form-control" id="price" placeholder="价格" path="price"/>
                        </div>
                        <label for="price" class="col-sm-4 control-label" style="text-align: left; padding-left: 0px">元 / 天</label>
                    </div>
                    <div class="form-group">
                        <label for="typeId" class="col-sm-3 control-label">器材类型：</label>
                        <div class="col-sm-5">
                            <select  id="typeId" name="typeId" class="form-control">
                                <option value="">请选择类型</option>
                                <jstl:forEach items="${types}" var="item">
                                    <option value="${item.id}">${item.name}</option>
                                </jstl:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">详细描述：</label>
                        <div class="col-sm-7">
                            <fo:textarea cssClass="form-control" id="description" path="description"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">提交</button>
                        </div>
                    </div>
                </fo:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
