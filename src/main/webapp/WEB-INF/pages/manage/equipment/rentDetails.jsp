<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
  <title>预留/预约情况</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
  <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
  <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
  <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
  <div class="right-content">
    <div style="height: 50px; margin-top: 4px">
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/manage">主页</a></li>
        <li>场地管理</li>
        <li class="active">预留/预约情况</li>
      </ol>
    </div>
    <div class="wrap-table">
      <nav class="navbar navbar-default" role="navigation">
        <div>
          <div>
            <fo:form action="/manage/competition/search" cssClass="navbar-form navbar-left" role="search"
                     method="post">
              <div class="form-group">
                <input type="text" class="form-control" name="queryStr" placeholder="输入查询条件"
                       value="${queryStr}">
              </div>
              <button type="submit" class="btn btn-default">提交</button>
            </fo:form>
          </div>
        </div>
      </nav>
      <table class="table table-bordered table-striped table-hover table-simple">
        <thead>
        <tr>
          <th>id</th>
          <th>租借时间</th>
          <th>归还时间</th>
          <th>租借人</th>
          <th>联系电话</th>
          <th>租借器材</th>
          <th>总金额</th>
          <th>罚款金额</th>
          <th>状态</th>
          <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <jstl:forEach items="${page.recordList}" var="item">
          <tr>
            <td>${item.id}</td>
            <td><spring:eval expression="item.begin"/></td>
            <td><spring:eval expression="item.end"/></td>
            <td>${item.student.name}</td>
            <td>${item.telephone}</td>
            <td>${item.equipment.type.name}-${item.equipment.label}</td>
            <td>${item.wholePrice}</td>
            <td>${item.publish}</td>
            <td>
              <jstl:choose>
                <jstl:when test="${item.status==0}">已预约</jstl:when>
                <jstl:when test="${item.status==2}">交易完成</jstl:when>
                <jstl:when test="${item.status==4}">已取消</jstl:when>
              </jstl:choose>
            </td>
            <td>
              <button>更新</button>
              <jstl:if test="${item.status==0||item.status==1}">
                <button>取消</button>
                <button>结账</button>
              </jstl:if>
            </td>
          </tr>
        </jstl:forEach>
        </tbody>
      </table>
      <div class="col-sm-12" style="text-align: center">
        <ul class="pagination">
          <jstl:if test="${page.currentPage>1}">
            <li><a href="${pageContext.request.contextPath}/manage/place/obligateDetails?pageNum=${page.currentPage-1}">&laquo;</a></li>
          </jstl:if>
          <jstl:if test="${page.beginIndex>2}">
            <li><a href="${pageContext.request.contextPath}/manage/place/obligateDetails?pageNum=1">1</a></li>
            <li><a href="#">...</a></li>
          </jstl:if>
          <jstl:forEach begin="${page.beginIndex}" end="${page.endIndex}" varStatus="step">
            <li class="${page.currentPage==step.index?'active':''}">
              <a href="${pageContext.request.contextPath}/manage/place/obligateDetails?pageNum=${step.index}">${step.index}</a>
            </li>
          </jstl:forEach>
          <jstl:if test="${page.endIndex+1<page.pageCount}">
            <li><a href="#">...</a></li>
            <li><a href="${pageContext.request.contextPath}/manage/place/obligateDetails?pageNum=${page.pageCount}">${page.pageCount}</a></li>
          </jstl:if>
          <jstl:if test="${page.endIndex<page.pageCount}">
            <li><a href="${pageContext.request.contextPath}/manage/place/obligateDetails?pageNum=${page.currentPage+1}">&raquo;</a></li>
          </jstl:if>
        </ul>
      </div>
    </div>
  </div>
</div>
</body>
</html>
