<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="utf-8" %>
<%@taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>器材类型</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage">主页</a></li>
                <li>系统管理</li>
                <li>类型管理</li>
                <li class="active">器材类型</li>
            </ol>
        </div>
        <div class="wrap-table">
            <nav class="navbar navbar-default" role="navigation">
                <button class="btn btn-primary" style="margin-top: 5px; margin-left: 20px" data-toggle="modal" data-target="#addModal">添加</button>
                <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <fo:form cssClass="form-horizontal" role="form" action="/manage/type/create/2" method="post">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">添加器材类型</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">类型名称：</label>
                                        <div class="col-sm-5">
                                            <input class="form-control" id="name" placeholder="请输入类型名称" name="name">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">关闭
                                    </button>
                                    <input type="submit" class="btn btn-primary" value="提交">
                                </div>
                            </fo:form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal -->
                </div>
            </nav>
            <table class="table table-bordered table-striped table-hover table-simple">
                <thead>
                <tr>
                    <th>id</th>
                    <th>名称</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <jstl:forEach items="${type}" var="item">
                    <tr>
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                        <td>
                            <button>更新</button>
                            <button data-opera="delete-type">删除</button>
                        </td>
                    </tr>
                </jstl:forEach>
                </tbody>
            </table>
            <%--删除类型的模态框--%>
            <div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="deleteModelTitle" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <fo:form cssClass="form-horizontal" role="form" action="/manage/type/delete/2" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    &times;
                                </button>
                                <h4 class="modal-title" id="deleteModelTitle">删除器材类型</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="typeId" class="col-sm-8 control-label">确认删除名称为    <span id="typeName"></span>    的器材类型</label>
                                    <div class="col-sm-1">
                                        <input type="hidden" class="form-control" id="typeId" placeholder="请输入类型名称" name="typeId">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                        data-dismiss="modal">取消
                                </button>
                                <input type="submit" class="btn btn-primary" value="确认">
                            </div>
                        </fo:form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal -->
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/type_m.js"></script>
</html>
