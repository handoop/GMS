<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="utf-8" %>
<%@taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>学生管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery.md5.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage">主页</a></li>
                <li>用户管理</li>
                <li class="active">学生管理</li>
            </ol>
        </div>
        <div class="wrap-table">
            <nav class="navbar navbar-default" role="navigation">
                <button class="btn btn-primary" style="margin-top: 5px; margin-left: 20px" data-toggle="modal"
                        data-target="#addModal">添加
                </button>
                <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <fo:form id="stuForm" cssClass="form-horizontal" role="form" action="/manage/student/register" method="post">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">添加场地类型</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">学生名称：</label>
                                        <div class="col-sm-5">
                                            <input class="form-control" id="name" placeholder="请输入学生名称" name="name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">账号：</label>
                                        <div class="col-sm-5">
                                            <input class="form-control" id="account" placeholder="账号" name="account">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">密码：</label>
                                        <div class="col-sm-5">
                                            <input class="form-control" id="pw" placeholder="密码">
                                            <input type="hidden" id="password" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">性别：</label>
                                        <div class="col-sm-5">
                                            <label class="checkbox-inline"><input type="radio" name="gender" value="1" checked>男</label>
                                            <label class="checkbox-inline"><input type="radio" name="gender" value="0">女</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button>
                                    <input type="submit" class="btn btn-primary" value="提交">
                                </div>
                            </fo:form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal -->
                </div>
            </nav>
            <table class="table table-bordered table-striped table-hover table-simple">
                <thead>
                <tr>
                    <th>id</th>
                    <th>名称</th>
                    <th>账号</th>
                    <th>性别</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <jstl:forEach items="${page.recordList}" var="item">
                    <tr>
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                        <td>${item.account}</td>
                        <td>${item.gender?'男':'女'}</td>
                        <td>
                            <button>更新</button>
                            <button data-opera="delete-type">删除</button>
                        </td>
                    </tr>
                </jstl:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/type_m.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#stuForm").validate({
            rules :{
                account : {
                    required : true
                },
                pw : {
                    required : true
                },
                name : {
                    required : true
                }
            },
            messages : {
                account : {
                    required : "不能为空"
                },
                pw : {
                    required : "不能为空"
                },
                name : {
                    required : "不能为空"
                }
            },
            submitHandler : function (form) {
                $("#password").val($.md5($("#pw").val()));
                form.submit();
            }
        })
    })
</script>
</html>
