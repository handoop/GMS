<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2015-1-1
  Time: 15:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags" %>
<div style="width: 14%; background-color: #FFFFFF; min-height: 100%;padding: 0px; margin: 0; float: left">
    <ul class="nav menu nav-pills nav-stacked">
        <li>
            <a href="#" data-toggle="collapse" data-target="#menu_item_1">系统管理</a>
            <ul id="menu_item_1"
                class="collapse nav nav-pills nav-stacked ${pageIndex==1000||pageIndex==1001||pageIndex==1002||pageIndex==1003||pageIndex==1004?'in':''}">
                <li class="${pageIndex==1000?'active':''}"><a href="#">角色管理</a></li>
                <li class="${pageIndex==1001?'active':''}"><a href="#">权限管理</a></li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#menu_option_1">类型管理</a>
                    <ul id="menu_option_1"
                        class="collapse nav nav-pills nav-stacked ${pageIndex==1002||pageIndex==1003?'in':''}">
                        <li class="${pageIndex==1002?'active':''}"><a href="${pageContext.request.contextPath}/manage/type/place">场地类型</a></li>
                        <li class="${pageIndex==1003?'active':''}"><a href="${pageContext.request.contextPath}/manage/type/equip">器材类型</a></li>
                    </ul>
                </li>
                <li class="${pageIndex==1004?'active':''}"><a href="#">罚款条例</a></li>
            </ul>
        </li>
        <li>
            <a href="#" data-toggle="collapse" data-target="#menu_item_6">用户管理</a>
            <ul id="menu_item_6" class="collapse nav nav-pills nav-stacked ${pageIndex==2200||pageIndex==2201?'in':''}">
                <li><a href="#">管理员管理</a></li>
                <li><a href="${pageContext.request.contextPath}/manage/student">学生管理</a></li>
            </ul>
        </li>
        <li>
            <a href="#" data-toggle="collapse" data-target="#menu_item_2">场地管理</a>
            <ul id="menu_item_2"
                class="collapse nav nav-pills nav-stacked ${pageIndex==1||pageIndex==2||pageIndex==3||pageIndex==4||pageIndex==5?'in':''}">
                <li class="${pageIndex==1?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/place/0">所有场地</a></li>
                <li class="${pageIndex==2?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/place/createPlace">添加场地</a></li>
                <li class="${pageIndex==3?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/place/search">查询场地</a></li>
                <li class="${pageIndex==4?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/place/obligate">预约/预留场地</a></li>
                <li class="${pageIndex==5?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/place/obligateDetails">预约/预留情况</a></li>
            </ul>
        </li>
        <li>
            <a href="#" data-toggle="collapse" data-target="#menu_item_3">赛事管理</a>
            <ul id="menu_item_3"
                class="collapse nav nav-pills nav-stacked ${pageIndex==100||pageIndex==101||pageIndex==102?'in':''}">
                <li class="${pageIndex==100?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/competition">所有赛事</a></li>
                <li class="${pageIndex==101?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/competition/create">创建赛事</a></li>
                <li class="${pageIndex==102?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/competition/search">查询赛事</a></li>
            </ul>
        </li>
        <li>
            <a href="#" data-toggle="collapse" data-target="#menu_item_4">器材管理</a>
            <ul id="menu_item_4"
                class="collapse nav nav-pills nav-stacked ${pageIndex==200||pageIndex==201||pageIndex==202||pageIndex==203||pageIndex==204?'in':''}">
                <li class="${pageIndex==200?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/equipment">所有器材</a></li>
                <li class="${pageIndex==201?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/equipment/create">新添器材</a></li>
                <li class="${pageIndex==202?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/equipment/search">查询器材</a></li>
                <li class="${pageIndex==203?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/equipment/rent">租借器材</a></li>
                <li class="${pageIndex==204?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/equipment/rentDetails">租借情况</a></li>
            </ul>
        </li>
        <li>
            <a href="#" data-toggle="collapse" data-target="#menu_item_5">公告管理</a>
            <ul id="menu_item_5"
                class="collapse nav nav-pills nav-stacked ${pageIndex==300||pageIndex==301||pageIndex==302?'in':''}">
                <li class="${pageIndex==300?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/announcement">所有公告</a></li>
                <li class="${pageIndex==301?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/announcement/create">创建公告</a></li>
                <li class="${pageIndex==302?'active':''}"><a
                        href="${pageContext.request.contextPath}/manage/announcement/search">查询公告</a></li>
            </ul>
        </li>
        <li><a href="#">数据统计和报表</a></li>
    </ul>
</div>
