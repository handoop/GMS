<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>所有场地</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage">主页</a></li>
                <li>场地管理</li>
                <li class="active">所有场地</li>
            </ol>
        </div>
        <div class="rwo">
            <ul class="nav nav-tabs">
                <jstl:forEach items="${types}" var="typeItem">
                    <li class="${typeItem.id==type?'active':''}">
                        <jstl:set var="url" value="${pageContext.request.contextPath}${'/manage/place/'}${typeItem.id}"></jstl:set>
                        <a href="${typeItem.id==type?'#show_tab':url}" data-toggler="tab">${typeItem.name}</a>
                    </li>
                </jstl:forEach>
            </ul>
            <div class="tab-content">
                <div class="tab-pane in active" id="show_tab"
                     style="background-color: #ffffff; min-height: 70%; padding: 8px 15px">
                    <table class="table table-bordered table-striped table-hover table-simple">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>编号</th>
                            <th>收费</th>
                            <th>状态</th>
                            <th>开放时间段</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <jstl:forEach items="${page.recordList}" var="item">
                            <tr>
                                <td>${item.id}</td>
                                <td>${item.label}</td>
                                <td>${item.price}</td>
                                <td>${item.status==0?'开放':'不开放'}</td>
                                <td>${item.openTime}</td>
                                <td>
                                    <button onclick="javascrip:window.location.href='${pageContext.request.contextPath}/manage/place/update/${item.id}'">更新</button>
                                    <button data-opera="delete-type">删除</button>
                                    <%--<button>预约</button>
                                    <button>预约情况</button>--%>
                                </td>
                            </tr>
                        </jstl:forEach>
                        </tbody>
                    </table>
                    <div class="col-sm-12" style="text-align: center">
                        <ul class="pagination">
                            <jstl:if test="${page.currentPage>1}">
                                <li><a href="${pageContext.request.contextPath}/manage/place/${type}?pageNum=${page.currentPage-1}">&laquo;</a></li>
                            </jstl:if>
                            <jstl:if test="${page.beginIndex>2}">
                                <li><a href="${pageContext.request.contextPath}/manage/place/${type}?pageNum=1">1</a></li>
                                <li><a href="#">...</a></li>
                            </jstl:if>
                            <jstl:forEach begin="${page.beginIndex}" end="${page.endIndex}" varStatus="step">
                                <li class="${page.currentPage==step.index?'active':''}">
                                    <a href="${pageContext.request.contextPath}/manage/place/${type}?pageNum=${step.index}">${step.index}</a>
                                </li>
                            </jstl:forEach>
                            <jstl:if test="${page.endIndex+1<page.pageCount}">
                                <li><a href="#">...</a></li>
                                <li><a href="${pageContext.request.contextPath}/manage/place/${type}?pageNum=${page.pageCount}">${page.pageCount}</a></li>
                            </jstl:if>
                            <jstl:if test="${page.currentPage<page.pageCount}">
                                <li><a href="${pageContext.request.contextPath}/manage/place/${type}?pageNum=${page.currentPage+1}">&raquo;</a></li>
                            </jstl:if>
                        </ul>
                    </div>
                    <%--删除场地的模态框--%>
                    <div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="deleteModelTitle" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <fo:form cssClass="form-horizontal" role="form" action="/manage/place/delete" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title" id="deleteModelTitle">删除场地</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="placeId" id="info" class="col-sm-8 control-label"></label>
                                            <div class="col-sm-1">
                                                <input type="hidden" class="form-control" id="placeId"  name="placeId">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default"
                                                data-dismiss="modal">取消
                                        </button>
                                        <input type="submit" class="btn btn-primary" value="确认">
                                    </div>
                                </fo:form>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/gms.js"></script>
</body>
</html>
