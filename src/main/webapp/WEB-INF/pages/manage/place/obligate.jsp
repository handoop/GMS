<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>所有场地</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
    <style type="text/css">
        .error{
            color: red;
            font-size: 10px;
        }
    </style>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div  class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage">主页</a></li>
                <li>场地管理</li>
                <li class="active">预约/预留场地</li>
            </ol>
        </div>
        <div class="rwo">
            <div style="width: 700px; margin: 0 auto;padding: 20px 30px;background-color: #FFFFFF; border-radius: 5px">
                <fo:form id="obligateForm" cssClass="form-horizontal" role="form" modelAttribute="rentPlace" action="/manage/place/obligate" method="post">
                    <div class="form-group">
                        <label for="typeId" class="col-sm-3 control-label">场地类型：</label>
                        <div class="col-sm-5">
                            <select  id="typeId" class="form-control">
                                <option>选择场地类型</option>
                                <jstl:forEach items="${types}" var="item">
                                    <option value="${item.id}">${item.name}</option>
                                </jstl:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="label" class="col-sm-3 control-label">场地编号：</label>
                        <div class="col-sm-5">
                            <select id="label" name="placeId" class="form-control">

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="begin" class="col-sm-3 control-label">时间段：</label>
                        <div class="col-sm-4">
                            <fo:input cssClass="form-control" id="begin" path="begin" placeholder="起始时间"/>
                        </div>
                        <label for="end" class="col-sm-1 control-label">—</label>
                        <div class="col-sm-4">
                            <fo:input cssClass="form-control" path="end" id="end" placeholder="结束时间"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 col-sm-offset-3" style="color: #d5d5d5;">格式0000-00-00 00:00:00</label>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-sm-3 control-label">预留/预约：</label>
                        <div class="col-sm-5">
                            <fo:select path="level" id="type" cssClass="form-control">
                                <fo:option value="0" selected="selected">预约</fo:option>
                                <fo:option value="1">预留</fo:option>
                            </fo:select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="account" class="col-sm-3 control-label">学生学号：</label>
                        <div class="col-sm-5">
                            <input class="form-control" name="account" id="account" placeholder="学生学号"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telephone" class="col-sm-3 control-label">联系电话：</label>
                        <div class="col-sm-5">
                            <fo:input cssClass="form-control" path="telephone" id="telephone" placeholder="联系电话"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">提交</button>
                        </div>
                    </div>
                </fo:form>
            </div>
        </div>
    </div>
</div>
</body>
<script style="text/javascript" src="${pageContext.request.contextPath}/js/pla_obli.js"></script>
</html>
