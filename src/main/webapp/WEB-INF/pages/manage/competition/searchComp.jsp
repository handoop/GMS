<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>查询赛事</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage/place">主页</a></li>
                <li><a href="#">赛事管理</a></li>
                <li class="active">查询赛事</li>
            </ol>
        </div>
        <div style="background-color: #FFFFFF; min-height: 70%; padding: 15px 20px">
            <nav class="navbar navbar-default" role="navigation">
                <div>
                    <fo:form action="/manage/competition/search" cssClass="navbar-form navbar-left" role="search"
                             method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="queryStr" placeholder="输入查询条件"
                                   value="${queryStr}">
                        </div>
                        <button type="submit" class="btn btn-default">提交</button>
                    </fo:form>
                </div>
            </nav>
            <table class="table table-bordered table-striped table-hover table-simple">
                <thead>
                <tr>
                    <th>id</th>
                    <th>赛事编号</th>
                    <th>赛事名称</th>
                    <th>举办时间</th>
                    <th>举办方</th>
                    <th>负责人</th>
                    <th>负责人电话</th>
                    <th>场地</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Tanmay</td>
                    <td>Bangalore</td>
                    <td>Bangalore</td>
                    <td>开放</td>
                    <td>
                        560001fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
                    </td>
                    <td>560001</td>
                    <td>560001</td>
                    <td>
                        <button>更新</button>
                    </td>
                    <td>
                        <button>更新</button>
                        <button>删除</button>
                        <button>预约</button>
                        <button>预约情况</button>
                    </td>
                </tr>
                <tr>
                    <td>Sachin</td>
                    <td>Mumbai</td>
                    <td>Mumbai</td>
                    <td>开放</td>
                    <td>开放</td>
                    <td>开放</td>
                    <td>开放</td>
                    <td>
                        <button>更新</button>
                    </td>
                    <td>
                        <button>更新</button>
                        <button>删除</button>
                        <button>预约</button>
                        <button>预约情况</button>
                    </td>
                </tr>
                <tr>
                    <td> Una</td>
                    <td>Uma</td>
                    <td>Pune</td>
                    <td>开放</td>
                    <td>开放</td>
                    <td>开放</td>
                    <td>开放</td>
                    <td>
                        <button>更新</button>
                    </td>
                    <td>
                        <button>更新</button>
                        <button>删除</button>
                        <button>预约</button>
                        <button>预约情况</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
