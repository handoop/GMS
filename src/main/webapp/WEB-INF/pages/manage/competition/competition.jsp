<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>所有赛事</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage">主页</a></li>
                <li>赛事管理</li>
                <li class="active">所有赛事</li>
            </ol>
        </div>
        <div class="wrap-table">
            <nav class="navbar navbar-default" role="navigation">
                <div>
                    <fo:form action="/manage/competition/search" cssClass="navbar-form navbar-left" role="search"
                             method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="queryStr" placeholder="输入查询条件"
                                   value="${queryStr}">
                        </div>
                        <button type="submit" class="btn btn-default">提交</button>
                    </fo:form>
                </div>
            </nav>
            <table class="table table-bordered table-striped table-hover table-simple">
                <thead>
                <tr>
                    <th>id</th>
                    <th>赛事编号</th>
                    <th>赛事名称</th>
                    <th>举办时间</th>
                    <th>举办方</th>
                    <th>负责人</th>
                    <th>负责人电话</th>
                    <th>场地</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <jstl:forEach items="${page.recordList}" var="item">
                    <tr>
                        <td>${item.id}</td>
                        <td>${item.label}</td>
                        <td>${item.name}</td>
                        <td><spring:eval expression="item.holdDate"/></td>
                        <td>${item.organizes}</td>
                        <td>${item.principal}</td>
                        <td>${item.principalTele}</td>
                        <td><button>查看</button><span class="badge">${item.rents.size()}</span></td>
                        <td>
                            <button>更新</button>
                            <button>删除</button>
                        </td>
                    </tr>
                </jstl:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
