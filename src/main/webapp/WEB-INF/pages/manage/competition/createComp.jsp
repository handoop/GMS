<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/12/24
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>创建赛事</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/m_style.css">
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
    <script style="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
    <style type="text/css">
        #pick-place > .form-group > div {
            padding-left: 0px;
            padding-right: 0px;
        }
        .error{
            color: red;
            font-size: 10px;
        }
    </style>
</head>
<body class="body">
<jsp:include page="/WEB-INF/pages/manage/public/header.jsp"/>
<div>
    <jsp:include page="/WEB-INF/pages/manage/public/menu.jsp"/>
    <div class="right-content">
        <div style="height: 50px; margin-top: 4px">
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/manage/place">主页</a></li>
                <li>赛事管理</li>
                <li class="active">创建赛事</li>
            </ol>
        </div>
        <div>
            <div style="width: 100%;margin: 0 auto; background-color: #ffffff; padding: 20px 30px; border-radius: 5px;">
                <fo:form id="createCompForm" cssClass="form-horizontal" role="form" modelAttribute="competition">
                    <div class="form-group">
                        <label for="label" class="col-sm-2 control-label">赛事编号：</label>
                        <div class="col-sm-3">
                            <fo:input cssClass="form-control" id="label" placeholder="请输入赛事编号" path="label"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">赛事名称：</label>
                        <div class="col-sm-3">
                            <fo:input cssClass="form-control" id="name" placeholder="赛事名称" path="name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="holdDate" class="col-sm-2 control-label">举办时间：</label>
                        <div class="col-sm-2">
                            <fo:input cssClass="form-control" id="holdDate" placeholder="举办时间" path="holdDate"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="organizes" class="col-sm-2 control-label">举办方：</label>

                        <div class="col-sm-7">
                            <fo:input cssClass="form-control" id="organizes" placeholder="举办方" path="organizes"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="principal" class="col-sm-2 control-label">负责人：</label>

                        <div class="col-sm-2">
                            <fo:input cssClass="form-control" id="principal" placeholder="负责人" path="principal"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="principalTele" class="col-sm-2 control-label">负责人电话：</label>
                        <div class="col-sm-3">
                            <fo:input cssClass="form-control" id="principalTele" placeholder="负责人电话"
                                      path="principalTele"/>
                        </div>
                    </div>
                    <div id="place-pick">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">选择场地：</label>
                        </div>
                        <div data-type="place-pick">
                            <div class="form-group">
                                <label for="placeType" class="col-sm-2 control-label">场地类型：</label>
                                <div style="width: 150px; display: inline-block">
                                    <select class="form-control" name="placeType" id="placeType">
                                        <option value="1" selected>选择场地类型</option>
                                        <jstl:forEach items="${types}" var="item">
                                            <option value="${item.id}">${item.name}</option>
                                        </jstl:forEach>
                                    </select>
                                </div>

                                <label for="placeLabel" class="control-label">场地编号：</label>
                                <div style="width: 130px; display: inline-block">
                                    <select class="form-control" name="placeId" id="placeLabel">

                                    </select>
                                </div>

                                <label for="begin" class="control-label">时间段：</label>
                                <div style="width: 150px; display: inline-block">
                                    <input class="form-control" id="begin" name="begin" placeholder="起始时间"/>
                                </div>
                                <label for="end" class="control-label">-</label>
                                <div style="width: 150px; display: inline-block">
                                    <input class="form-control" id="end" name="end" placeholder="结束时间"/>
                                </div>
                                <div  class="btn btn-danger" data-controll="place-pick" style="display: none; padding: 3px 5px">删除</div>
                            </div>
                        </div>
                        <div class="form-group" data-type="place-add-sign">
                            <div class="col-sm-offset-2 col-sm-9">
                                <div class="btn btn-primary" data-controll="place-add">添加占用场地</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                            <button type="submit" class="btn btn-default">提交</button>
                        </div>
                    </div>
                </fo:form>
            </div>
        </div>
    </div>
</div>
<script style="text/javascript" src="${pageContext.request.contextPath}/js/gms.js"></script>
</body>
</html>
