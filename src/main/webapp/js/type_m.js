/**
 * Created by Administrator on 2015-1-4.
 */
(function ($) {
    $(document).ready(function () {
        $("[data-opera='delete-type']").on("click", function () {
            var $this = $(this);
            var $parent = $this.parent().parent();
            $("#typeName").text($parent.find("td").eq(1).text());
            $("#typeId").val($parent.find("td").eq(0).text());
            $("#deleteModel").modal('show');
        });
    })
})(jQuery);