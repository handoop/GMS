/**
 * Created by Administrator on 2014/12/31.
 */

/*Place.jsp*/
(function ($) {
    $(document).ready(function () {
        $("[data-opera='delete-type']").on("click", function () {
            var $this = $(this);
            var type = $("[href='#show_tab']").text();
            var label = $this.parent().parent().find("td").eq(1).text();
            var placeId = $this.parent().parent().find("td").eq(0).text();
            $("#placeId").val(placeId);
            $("#info").text('确认删除 '+type+'-'+label+' 场地吗？');
            $("#deleteModel").modal("show");
        })
    })
})(jQuery);

/*创建赛事*/
(function ($) {
    $(document).ready(function () {
        $("[data-type='place-pick']").on("mouseenter", function () {
            $(this).find("[data-controll='place-pick']").show();
        });
        $("[data-type='place-pick']").on("mouseleave", function () {
            $(this).find("[data-controll='place-pick']").hide();
        });
        $("[data-controll='place-add']").on("click", function () {
            $("[data-type='place-add-sign']").before($("[data-type='place-pick']").clone(true)[0]);
        });
        $("[data-controll='place-pick']").on("click", function () {
            var $this = $(this);
            $this.parent().remove();
        })
    })
})(jQuery);

/*场地预约页面，判断预约日期是否有冲突*/
(function ($) {
    $(document).ready(function () {
        $("#placeType").on("change", function () {
            var $this = $(this);
            var $parent = $this.parent().parent();
            var $placeLabel = $parent.find("#placeLabel");
            $placeLabel.find("option").remove();
            var value = $this.val();
            $.ajax({
                url : "/place/obligateLabel",
                type : "GET",
                cache : false,
                data : {typeId : value},
                dataType : "json",
                success : function(data){
                    if(data.response){
                        for(var i = 0; data.places.length; i++){
                            var $option = $("<option value='"+data.places[i].id+"'>"+data.places[i].label+"</option>");
                            $placeLabel.append($option);
                        }
                    }else{
                        alert("服务器繁忙");
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown){
                    alert("服务器异常！");
                }
            })
        });

    })
})(jQuery);