/**
 * Created by Administrator on 2015-1-5.
 */
(function ($) {
    $(document).ready(function () {
        $("#typeId").on("change", function () {
            var $this = $(this);
            $("#label").find("option").remove();
            var value = $this.val();
            $.ajax({
                url : "/equipment/obtainLabel",
                type : "GET",
                cache : false,
                data : {typeId : value},
                dataType : "json",
                success : function(data){
                    if(data.response){
                        for(var i = 0; data.equips.length; i++){
                            var $option = $("<option value='"+data.equips[i].id+"'>"+data.equips[i].label+"</option>");
                            $("#label").append($option);
                        }
                    }else{
                        alert("服务器繁忙");
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown){
                    alert("服务器异常！");
                }
            })
        });
    })
})(jQuery);