/**
 * Created by Administrator on 2015-1-5.
 */
(function ($) {
    $(document).ready(function () {
        $("#typeId").on("change", function () {
            var $this = $(this);
            $("#label").find("option").remove();
            var value = $this.val();
            $.ajax({
                url : "/place/obligateLabel",
                type : "GET",
                cache : false,
                data : {typeId : value},
                dataType : "json",
                success : function(data){
                    if(data.response){
                        for(var i = 0; data.places.length; i++){
                            var $option = $("<option value='"+data.places[i].id+"'>"+data.places[i].label+"</option>");
                            $("#label").append($option);
                        }
                    }else{
                        alert("服务器繁忙");
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown){
                    alert("服务器异常！");
                }
            })
        });

        /*表单效验*/
        $("#obligateForm").validate({
            rules :{
                begin : {
                    date : true
                },
                end : {
                    date : true,
                    remote : {
                        url : "/place/obligateValDate",
                        type : "post",
                        dataType : "json",
                        data : {
                            placeId : function () {
                                return $("#label").val();
                            },
                            begin : function(){
                                return $("#begin").val();
                            },
                            end : function(){
                                return $("#end").val();
                            }
                        }
                    }
                }
            },
            messages : {
                begin : {
                    date : "请输入正确日期格式的日期"
                },
                end : {
                    date : "请输入正确日期格式的日期",
                    remote : "这个时间已被预约"
                }
            }
        })

    })
})(jQuery);