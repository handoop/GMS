package com.handoop.gms.test;


import com.handoop.gms.domain.PlaceType;
import com.handoop.gms.domain.Student;
import com.handoop.gms.service.RentPlaceService;
import com.handoop.gms.service.TypeService;
import com.handoop.gms.service.UserService;
import com.handoop.gms.service.imp.TypeServiceImp;
import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Administrator on 2014/12/23.
 */
public class Test {

    private ApplicationContext context ;

    @Before
    public void prepare(){
        context = new ClassPathXmlApplicationContext("spring-context.xml");
    }

    @org.junit.Test
    public void test1(){
        UserService service = (UserService) context.getBean("userServiceImp");
        Student student = new Student();
        student.setName("hand");
        student.setAccount("123");
        student.setPassword("123");
    }

    @org.junit.Test
    public void test2(){
        TypeService service = new TypeServiceImp<PlaceType>();
    }

    @org.junit.Test
    public void test3(){
        String value = " 00   89 8 9         ";
         value = value.replaceAll("\\s+", "*");
        System.out.println(value);
    }
}
