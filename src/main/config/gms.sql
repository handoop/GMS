﻿# Host: localhost  (Version: 5.6.17)
# Date: 2015-01-06 22:13:03
# Generator: MySQL-Front 5.3  (Build 4.121)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "announcement"
#

DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "announcement"
#


#
# Structure for table "competition"
#

DROP TABLE IF EXISTS `competition`;
CREATE TABLE `competition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `label` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `organizes` varchar(100) NOT NULL,
  `holdDate` datetime NOT NULL,
  `principalTele` varchar(20) NOT NULL,
  `principal` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Data for table "competition"
#

INSERT INTO `competition` VALUES (1,'Autumn','HK556',0,'广东海洋大学','2015-01-21 00:00:00','13824843156','handoop'),(2,'Spring','HK56',0,'广东海洋大学','2015-01-21 00:00:00','13824843156','handoop'),(3,'handoop','AK11',0,'广东海洋大学','2015-01-21 00:00:00','13824843156','handoop'),(6,'国家级羽毛球比赛','HK110',0,'广东海洋大学','2015-01-21 00:00:00','13824843156','handoop');

#
# Structure for table "privilege"
#

DROP TABLE IF EXISTS `privilege`;
CREATE TABLE `privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `url` varchar(100) NOT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "privilege"
#


#
# Structure for table "role"
#

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "role"
#


#
# Structure for table "manager"
#

DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `account` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `gender` bit(1) NOT NULL DEFAULT b'0',
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `roleId` (`roleId`),
  CONSTRAINT `emanager_role_FK` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "manager"
#


#
# Structure for table "role_privilege"
#

DROP TABLE IF EXISTS `role_privilege`;
CREATE TABLE `role_privilege` (
  `roleId` bigint(20) NOT NULL,
  `privilegeId` bigint(20) NOT NULL,
  PRIMARY KEY (`roleId`,`privilegeId`),
  UNIQUE KEY `roleId` (`roleId`),
  UNIQUE KEY `privilegeId` (`privilegeId`),
  CONSTRAINT `role_privilege_Privilege_FK` FOREIGN KEY (`privilegeId`) REFERENCES `privilege` (`id`),
  CONSTRAINT `role_privilege_Role_FK` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "role_privilege"
#


#
# Structure for table "student"
#

DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `account` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL DEFAULT '',
  `gender` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "student"
#

INSERT INTO `student` VALUES (1,'handoop','201211701424','13824843156',b'0'),(2,'claire','123','202cb962ac59075b964b07152d234b70',b'1');

#
# Structure for table "type_equip"
#

DROP TABLE IF EXISTS `type_equip`;
CREATE TABLE `type_equip` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Data for table "type_equip"
#

INSERT INTO `type_equip` VALUES (7,'羽毛球','2015-01-06 15:27:21'),(8,'篮球','2015-01-06 15:27:28'),(9,'乒乓球','2015-01-06 15:27:36');

#
# Structure for table "equipment"
#

DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(500) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `createDate` datetime NOT NULL,
  `typeId` bigint(20) NOT NULL,
  `label` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `typeId` (`typeId`),
  CONSTRAINT `equipment_type_FK` FOREIGN KEY (`typeId`) REFERENCES `type_equip` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "equipment"
#

INSERT INTO `equipment` VALUES (1,'哈哈',0,10.00,'2015-01-06 16:25:53',7,'HH001'),(2,'哇咔咔',0,10.00,'2015-01-06 16:57:14',7,'HH002');

#
# Structure for table "rent_equip"
#

DROP TABLE IF EXISTS `rent_equip`;
CREATE TABLE `rent_equip` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `begin` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `telephone` varchar(20) NOT NULL,
  `studentId` bigint(20) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `createDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `wholePrice` decimal(10,2) DEFAULT '0.00',
  `publish` decimal(10,2) DEFAULT '0.00',
  `equipId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `studentId` (`studentId`),
  KEY `rent_equip_FK_equip` (`equipId`),
  CONSTRAINT `rent_equip_FK_equip` FOREIGN KEY (`equipId`) REFERENCES `equipment` (`id`),
  CONSTRAINT `rent_equip_equip_FK` FOREIGN KEY (`studentId`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "rent_equip"
#

INSERT INTO `rent_equip` VALUES (1,'2015-01-06 19:46:12',NULL,'13824843156',1,0,'2015-01-06 19:46:12',0.00,0.00,1);

#
# Structure for table "type_place"
#

DROP TABLE IF EXISTS `type_place`;
CREATE TABLE `type_place` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

#
# Data for table "type_place"
#

INSERT INTO `type_place` VALUES (11,'羽毛球场','2015-01-04 15:41:29'),(12,'乒乓球场','2015-01-04 15:41:39'),(13,'篮球场','2015-01-04 15:41:47'),(14,'健身房','2015-01-04 15:41:55'),(15,'桌球场','2015-01-04 15:42:12');

#
# Structure for table "place"
#

DROP TABLE IF EXISTS `place`;
CREATE TABLE `place` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `price` decimal(10,2) DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `typeId` bigint(20) NOT NULL,
  `openTime` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label` (`label`),
  KEY `typeId` (`typeId`),
  CONSTRAINT `Place_Type_FK` FOREIGN KEY (`typeId`) REFERENCES `type_place` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Data for table "place"
#

INSERT INTO `place` VALUES (1,'AK88',0,10.00,'2015-01-04 19:42:34',11,'08:00-11:00,14:00-17:00'),(2,'AK01',0,10.00,'2015-01-05 12:44:25',11,'08:00-11:00,14:00-17:00'),(3,'AK02',0,10.00,'2015-01-05 12:44:33',11,'08:00-11:00,14:00-17:00'),(4,'AK03',0,10.00,'2015-01-05 12:44:41',11,'08:00-11:00,14:00-17:00'),(5,'AK04',0,10.00,'2015-01-05 12:44:48',11,'08:00-11:00,14:00-17:00'),(6,'AK05',0,10.00,'2015-01-05 12:44:56',11,'08:00-11:00,14:00-17:00');

#
# Structure for table "rent_place"
#

DROP TABLE IF EXISTS `rent_place`;
CREATE TABLE `rent_place` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `begin` datetime NOT NULL,
  `end` datetime NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `telephone` varchar(20) DEFAULT '',
  `placeId` bigint(20) NOT NULL,
  `competitionId` bigint(20) DEFAULT NULL,
  `studentId` bigint(20) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `createDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `wholePrice` decimal(10,2) DEFAULT '0.00',
  `publish` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `competitionId` (`competitionId`),
  KEY `placeId` (`placeId`),
  KEY `studentId` (`studentId`),
  CONSTRAINT `rent_place_competition_FK` FOREIGN KEY (`competitionId`) REFERENCES `competition` (`id`),
  CONSTRAINT `rent_place_place_FK` FOREIGN KEY (`placeId`) REFERENCES `place` (`id`),
  CONSTRAINT `rent_place_student_FK` FOREIGN KEY (`studentId`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

#
# Data for table "rent_place"
#

INSERT INTO `rent_place` VALUES (2,'2015-01-06 08:00:00','2015-01-06 11:00:00',0,'13824843156',2,NULL,1,0,'2015-01-05 15:16:41',0.00,0.00),(3,'2015-01-07 08:00:00','2015-01-07 10:00:00',0,'13824843156',2,NULL,1,0,'2015-01-05 16:37:56',0.00,0.00),(4,'2015-01-07 14:00:00','2015-01-07 17:00:00',1,'13824843156',2,NULL,1,0,'2015-01-05 16:40:18',0.00,0.00),(5,'2015-01-21 08:00:00','2015-01-21 11:00:00',1,NULL,2,6,NULL,0,'2015-01-06 14:04:46',0.00,0.00),(6,'2015-01-21 08:00:00','2015-01-21 11:00:00',1,NULL,2,6,NULL,0,'2015-01-06 14:04:46',0.00,0.00),(7,'2015-01-21 08:00:00','2015-01-21 11:00:00',1,NULL,3,6,NULL,0,'2015-01-06 14:04:47',0.00,0.00),(8,'2015-01-21 08:00:00','2015-01-21 11:00:00',1,NULL,2,6,NULL,0,'2015-01-06 14:04:46',0.00,0.00),(9,'2015-01-21 08:00:00','2015-01-21 11:00:00',1,NULL,3,6,NULL,0,'2015-01-06 14:04:47',0.00,0.00),(10,'2015-01-22 08:00:00','2015-01-21 10:00:00',1,NULL,3,6,NULL,0,'2015-01-06 14:04:47',0.00,0.00);
